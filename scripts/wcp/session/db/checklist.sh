# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

db_session_checklist_new() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "INSERT INTO checklist (\"session-id\") VALUES (${session_id});"
}

db_session_checklist_info_get() {
  local session_id
  session_id="${1}"

  (
    cat <<EOF
SELECT "interest-check", "previous-pick-pr",
       "self-pr", "ci-passed",
       "e2e-label-pr", "e2e-result-action-run",
       "reviewers-assigned", "matrix-48h-call-url",
       "release-notes" FROM checklist WHERE "session-id"=${session_id};
EOF
  ) | sqlite3 "${DBFILE}"
}

db_session_checklist_self_pr_get() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT \"self-pr\" FROM checklist WHERE \"session-id\"=${session_id}"
}

db_session_checklist_update_field() {
  local session_id field value
  session_id="${1}"
  field="${2}"
  value="${3}"

  sqlite3 "${DBFILE}" "UPDATE checklist SET \"${field}\"='${value}' WHERE \"session-id\"=${session_id};"
}

db_session_checklist_get_field() {
  local session_id field value
  session_id="${1}"
  field="${2}"

  sqlite3 "${DBFILE}" "SELECT \"${field}\" FROM checklist WHERE \"session-id\"=${session_id};"
}

db_session_find_previous_pr() {
  local session_id source target from
  session_id="${1}"
  source="${2}"
  target="${3}"
  from="${4}"

  sqlite3 "${DBFILE}" \
    "SELECT \"self-pr\" FROM checklist \
     LEFT JOIN session ON checklist.\"session-id\" = session.id \
     WHERE session.\"source-end-ref\" = '${from}' AND \
           session.\"source-branch\" = '${source}' AND \
           session.\"target-base-branch\" = '${target}' AND \
           session.id != '${session_id}';"
}
