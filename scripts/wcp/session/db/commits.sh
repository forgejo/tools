# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

db_session_commit_add_new() {
  local session_id n commit_id
  session_id="${1}"
  n="${2}"
  commit_id="${3}"

  sqlite3 "${DBFILE}" "INSERT INTO commits (\"session-id\", \"source-commit-id\", \"n\") VALUES ('${session_id}', '${commit_id}', '${n}');"
}

db_session_commits_list() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT \"source-commit-id\" FROM commits WHERE \"session-id\"=${session_id} ORDER BY n ASC;"
}

db_session_commits_list_marked() {
  local session_id mark
  session_id="${1}"
  mark="${2}"

  sqlite3 "${DBFILE}" "SELECT \"source-commit-id\" FROM commits WHERE \"session-id\"=${session_id} AND mark='${mark}' ORDER BY n ASC;"
}

db_session_commits_count() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT COUNT(n) FROM commits WHERE \"session-id\"=${session_id};"
}

db_session_commits_count_marked() {
  local session_id mark
  session_id="${1}"
  mark="${2}"

  sqlite3 "${DBFILE}" "SELECT COUNT(n) FROM commits WHERE \"session-id\"=${session_id} AND mark='${mark}';"
}

db_session_commits_list_handled() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT \"source-commit-id\" FROM commits WHERE \"session-id\"=${session_id} AND mark IN ('pick', 'todo') ORDER BY n DESC;"
}

db_session_commits_list_skipped() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT \"source-commit-id\" FROM commits WHERE \"session-id\"=${session_id} AND mark = 'skip' ORDER BY n DESC;"
}

db_session_commits_list_work() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT \"source-commit-id\" FROM commits WHERE \"session-id\"=${session_id} AND mark IN ('port', 'in-progress') ORDER BY n DESC;"
}

db_session_commits_count_already_in_forgejo() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT COUNT(*) FROM commits WHERE \"session-id\"=${session_id} AND mark='skip' AND comments LIKE '%Already in Forgejo%'"
}
