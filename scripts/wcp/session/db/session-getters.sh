# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

db_session_get_id() {
  sqlite3 "${DBFILE}" "SELECT id FROM session ORDER BY id DESC LIMIT 1;"
}

db_session_get_new_id() {
  sqlite3 "${DBFILE}" "INSERT INTO session DEFAULT VALUES;"
  db_session_get_id
}

db_session_target_base_branch_get() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT \"target-base-branch\" FROM session WHERE id=${session_id};"
}

db_session_source_branch_get() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT \"source-branch\" FROM session WHERE id=${session_id};"
}

db_session_get_info() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT \"id\", \"week\", \"target-base-branch\", \"target-base-ref\", \"source-branch\", \"source-start-ref\", \"source-end-ref\", \"locked\" FROM session WHERE id=${session_id};"
}

db_session_week_get() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT week FROM session WHERE id=${session_id}"
}

db_session_get_note() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT note FROM session WHERE id=${session_id};"
}

db_session_source_end_ref_get() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT \"source-end-ref\" FROM session WHERE id=${session_id}"
}

db_session_source_start_ref_get() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT \"source-start-ref\" FROM session WHERE id=${session_id}"
}

db_session_get_locked_source_end_ref() {
  local source_branch target_branch
  source_branch="${1}"
  target_branch="${2}"

  sqlite3 "${DBFILE}" "SELECT \"source-end-ref\" FROM session WHERE locked=1 AND \"source-branch\" = '${source_branch}' AND \"target-base-branch\"='${target_branch}' ORDER BY id DESC LIMIT 1;"
}

db_session_get_unlocked_id_for() {
  local source_branch target_branch
  source_branch="${1}"
  target_branch="${2}"

  sqlite3 "${DBFILE}" "SELECT \"id\" FROM session WHERE locked=0 AND \"source-branch\" = '${source_branch}' AND \"target-base-branch\"='${target_branch}' ORDER BY id DESC LIMIT 1;"
}
