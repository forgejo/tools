# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

session_render_commits_handled() {
  local session_id
  session_id="${1}"

  commits="$(db_session_commits_list_handled "${session_id}")"

  echo "## Commits"
  echo
  for commit_id in ${commits}; do
    if ! commit_has_parents "${session_id}" "${commit_id}"; then
      commit_render "${session_id}" "${commit_id}"
    fi
  done
}

session_render_commits_todo() {
  local session_id
  session_id="${1}"

  commits="$(db_session_commits_list_work "${session_id}")"

  if [ -z "${commits}" ]; then
    return
  fi

  echo "## TODO"
  echo
  for commit_id in ${commits}; do
    if ! commit_has_parents "${session_id}" "${commit_id}"; then
      commit_render "${session_id}" "${commit_id}"
      echo "------"
    fi
  done
}

session_render_commits_skipped() {
  local session_id
  session_id="${1}"

  commits="$(db_session_commits_list_skipped "${session_id}")"

  if [ -z "${commits}" ]; then
    return
  fi

  echo "## Skipped"
  echo
  for commit_id in ${commits}; do
    if ! commit_has_parents "${session_id}" "${commit_id}"; then
      commit_render "${session_id}" "${commit_id}"
      echo "------"
    fi
  done
}

session_render_dashboard() {
  local session_id
  session_id="${1}"

  commits="$(db_session_commits_list_marked "${session_id}" "port")"

  if [ -z "${commits}" ]; then
    return
  fi

  local week self_pr
  week="$(db_session_week_get "${session_id}")"
  self_pr="$(db_session_checklist_self_pr_get "${session_id}")"

  echo "## [Week ${week}]($(commit_url_forgejo_pr "${self_pr}"))"
  echo
  for commit_id in ${commits}; do
    if ! commit_has_parents "${session_id}" "${commit_id}"; then
      commit_render "${session_id}" "${commit_id}"
      echo
    fi
  done
}

session_render_checklist() {
  local session_id
  session_id="${1}"

  echo "## Checklist"
  echo
  cmd_wcp_session_checklist_view "${session_id}"
}

session_render_legend() {
  cat <<EOF
## Legend

- :question: - No decision about the commit has been made.
- :cherries: - The commit has been cherry picked.
- :fast_forward: - The commit has been skipped.
- :bulb: - The commit has been skipped, but should be ported to Forgejo.
- :writing_hand: - The commit has been skipped, and a port to Forgejo already exists.
EOF
}

session_render_gitea_commit_link() {
  local commit_id short_id
  commit_id="${1}"
  # shellcheck disable=SC2001
  short_id="$(echo "${commit_id}" | sed -e "s,^\(.\{10\}\).*,\1,")"

  echo "[\`gitea@${short_id}\`]($(commit_url_gitea_commit "${commit_id}"))"
}

session_render_stats() {
  local session_id
  session_id="${1}"

  local commit_count picked_count skipped_count port_count already_in_forgejo_count skip_extra_text
  commit_count="$(db_session_commits_count "${session_id}")"
  picked_count="$(db_session_commits_count_marked "${session_id}" pick)"
  skipped_count="$(db_session_commits_count_marked "${session_id}" skip)"
  port_count="$(db_session_commits_list_work "${session_id}" | wc -l)"
  already_in_forgejo_count="$(db_session_commits_count_already_in_forgejo "${session_id}")"

  if [ "${already_in_forgejo_count}" -gt 0 ]; then
    local waswere
    waswere="were"
    if [ "${already_in_forgejo_count}" -eq 1 ]; then
      waswere="was"
    fi
    skip_extra_text=" (of which **${already_in_forgejo_count}** ${waswere} already in Forgejo!)"
  else
    skip_extra_text=""
  fi

  cat <<EOF
<details>
<summary><h2>Stats</h2></summary>

<br>

Between $(session_render_gitea_commit_link "$(db_session_source_start_ref_get "${session_id}")") and $(session_render_gitea_commit_link "$(db_session_source_end_ref_get "${session_id}")"), **${commit_count}** commits have been reviewed. We picked **${picked_count}**, skipped **${skipped_count}**${skip_extra_text}, and decided to port **${port_count}**.

</details>
EOF
}
