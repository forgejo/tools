# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

. "${_libdir}/session/cmd/start.sh"
. "${_libdir}/session/cmd/list-commits.sh"
. "${_libdir}/session/cmd/info.sh"
. "${_libdir}/session/cmd/lock_set.sh"
. "${_libdir}/session/cmd/delete.sh"
. "${_libdir}/session/cmd/preview.sh"
. "${_libdir}/session/cmd/pr.sh"
. "${_libdir}/session/cmd/add-commits.sh"
. "${_libdir}/session/cmd/add-commit.sh"
. "${_libdir}/session/cmd/checklist.sh"
. "${_libdir}/session/cmd/rebase.sh"
. "${_libdir}/session/cmd/ci.sh"
. "${_libdir}/session/cmd/release-notes.sh"

cmd_wcp_session() {
  local session_id

  case "${1:-}" in
    --id=*)
      session_id="${1/--id=/}"
      shift
      ;;
  esac

  local subcmd
  subcmd="${1:-}"
  shift || true

  if [ -z "${session_id:-}" ]; then
    if [ "${subcmd}" != "start" ]; then
      session_id="$(db_session_get_id)"
    fi
  fi

  case "${subcmd}" in
    start)
      cmd_wcp_session_start "${session_id:-}" "$@"
      ;;
    info | i)
      cmd_wcp_session_info "${session_id}" "$@"
      ;;
    lock | l)
      cmd_wcp_session_lock_set "${session_id}" 1
      ;;
    unlock)
      cmd_wcp_session_lock_set "${session_id}" 0
      ;;
    delete | d)
      cmd_wcp_session_delete "${session_id}" "$@"
      ;;
    preview | v)
      cmd_wcp_session_preview "${session_id}" "$@"
      ;;
    pr)
      cmd_wcp_session_pr "${session_id}" "$@"
      ;;
    ci)
      cmd_wcp_session_ci "${session_id}" "$@"
      ;;
    list-commits | ls)
      cmd_wcp_session_list_commits "${session_id}" "$@"
      ;;
    pending | p)
      cmd_wcp_session_list_commits "${session_id}" --mark=todo
      ;;
    add-commits)
      cmd_wcp_session_add_commits "${session_id}" "$@"
      ;;
    add-commit)
      cmd_wcp_session_add_commit "${session_id}" "$@"
      ;;
    rebase)
      cmd_wcp_session_rebase "${session_id}" "$@"
      ;;
    checklist | cl)
      cmd_wcp_session_checklist "${session_id}" "$@"
      ;;
    release-notes | rn)
      cmd_wcp_session_release_notes "${session_id}" "$@"
      ;;
    help | h)
      cmd_wcp_session_help
      ;;
    *)
      echo "Unknown subcommand: '${subcmd}'" >&2
      cmd_wcp_session_help >&2
      exit 1
      ;;
  esac
}

cmd_wcp_session_help() {
  cat <<EOF
wcp session [--id=<ID>] <SUBCOMMAND> ...

Aliases:
  wcp s

Subcommands:
  start [--week=<YEAR>-<WEEK>] [--note=<NOTE>] [--from=<FROM>] [--target=<BRANCH>] [--source=<REMOTE/BRANCH>]
    Starts a new session, all parameters are optional. Prints the newly started session's ID.

    Options:
      --week=<YEAR>-<WEEK>    : Start the session for <YEAR>'s <WEEK>th week of cherry picking.
                                Defaults to: $(session_default_week)
      --note=<NOTE>           : An optional note to place into the pull request's title.
      --from=<FROM>           : Git commit ref to start the cherry pick from.
                                Defaults to: <last week's commit>
      --target=<BRANCH>       : Target base branch to cherry pick onto.
                                Defaults to: forgejo
      --source=<REMOTE/BRANCH>: Branch to cherry pick from.
                                Defaults to: gitea/main

  add-commits [--until=<REF>]
    Adds more commits to the current session, between the current last one, and <REF>.
    Will record <REF> as the new last commit in the session.

    Options:
      --until=<REF>: Adds commits from the source branch up until <REF>.
                     Defaults to the top commit of the source branch.

  add-commit <REF>
    Add a single commit to the current session, without changing the last commit reference.
    This means that the picked <REF> will also be part of a future cherry pick.

  rebase [--onto=<REF>]
    Rebases the current state on top of <REF>.

    Options:
      --onto=<REF>: Ref to rebase onto.
                    Defaults to the original target branch.

  pr ...
    Handles opening and syncing with the ongoing pull request for the session.
    See \`wcp session pr help\` for more information.

  ci ...
    View the CI status of the session (if there's a PR open already), and
    retrieve logs for local inspection.
    See \`wcp session ci help\` for more information.

  lock
  l
    Lock the current session.
  unlock
    Unlocks the current session.

  delete
  d
    Delete the current session from the database.

  info
  i
    Displays information about the current session.

  preview [--dashboard]
  v       ...
    Preview the session's markdown document.

    If the --dashboard option is specified, render a preview snippet suitable
    for the "Gitea PR Porting Dashboard" issue (#3239).

  checklist [<SUBCOMMAND>]
  cl        ...
    Checklist handling. See \`wcp session checklist help\` for more information.

  release-notes | rn
    Create or edit the release notes for the session.

    Editing release notes will create a new commit, it should be merged back into
    the first release notes commit of the session manually.

  list-commits [--mark=todo|pick|skip|port|in-progress]
  ls           ...
    Lists commits in the session. If no mark is specified, lists them all.
  pending
  p
    Alias for \`wcp session list-commits --mark=todo\`.
EOF
}
