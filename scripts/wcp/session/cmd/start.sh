# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_session_start() {
  local session_id
  session_id="${1:-}"
  shift || true

  local week note from target source
  week="$(session_default_week)"
  target="forgejo"
  source="gitea/main"
  note=""

  # Process options
  while [ -n "${1:-}" ]; do
    case "$1" in
      --week=*)
        week="${1/--week=/}"
        ;;
      --note=*)
        note="${1/--note=/}"
        ;;
      --from=*)
        from="${1/--from=/}"
        ;;
      --target=*)
        target="${1/--target=/}"
        ;;
      --source=*)
        source="${1/--source=/}"
        ;;
    esac
    shift || true
  done

  # Verify that our branhes exist
  if ! git_branch_exists "${target}"; then
    echo "Error: target branch does not exist: '${target}'" >&2
    exit 1
  fi
  if ! git_branch_exists "${source}"; then
    echo "Error: source branch does not exist: '${source}'" >&2
    exit 1
  fi

  # Figure out the session id
  unlocked_id="$(db_session_get_unlocked_id_for "${source}" "${target}")"
  if [ -n "${unlocked_id}" ]; then
    echo "Session for (${source} -> ${target}) already in progress: ${unlocked_id}." >&2
    exit 1
  fi

  if [ -z "${session_id}" ]; then
    session_id="$(db_session_get_new_id)"
  fi

  # Bail out if already in progress
  if db_session_started "${session_id}"; then
    echo "Session #${session_id} already started, can't start again." >&2
    exit 1
  fi

  # Figure out where to start from...
  if [ -z "${from:-}" ]; then
    from="$(db_session_get_locked_source_end_ref "${source}" "${target}")"
  fi

  if [ -z "${from}" ]; then
    from="$(git_ref_get "${target}")"
  fi

  if [ -z "${from}" ]; then
    echo "Error: wcp session start: Unable to figure out where to start from. Please specify --from!" >&2
    exit 1
  fi

  # Update the session in the db with the params we know so far
  notefile="$(mktemp)"
  echo "${note}" >"${notefile}"
  db_session_update_params "${session_id}" "${week}" "${source}" "${target}" "${from}" "${notefile}"
  rm -f "${notefile}"

  # ...figure out the rest!
  local target_base_ref source_end_ref
  target_base_ref="$(git_ref_get "${target}")"
  source_end_ref="$(git_ref_get "${source}")"

  db_session_update_refs "${session_id}" "${target_base_ref}" "${source_end_ref}"

  # Populate the commits...
  local commits
  commits="$(git_commit_list_range "${from}" "${source_end_ref}")"

  n=0
  for commit_id in ${commits}; do
    db_session_commit_add_new "${session_id}" "${n}" "${commit_id}"
    n=$((n + 1))
  done

  # Create the checklist entry
  db_session_checklist_new "${session_id}"

  # Find the previous PR, and update the checklist
  db_session_checklist_update_field "${session_id}" "previous-pick-pr" "$(db_session_find_previous_pr "${session_id}" "${source}" "${target}" "${from}")"

  # Create the branch...
  git_branch_create_and_switch "wcp/${week}" "${target}"

  # ...print the session id
  echo "${session_id}"
}
