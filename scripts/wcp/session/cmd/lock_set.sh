# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_session_lock_set() {
  local session_id lock_value
  session_id="${1}"
  lock_value="${2}"

  if [ -z "${session_id}" ]; then
    echo "Error: Cannot lock/unlock a session without a session id." >&2
    exit 1
  fi

  session_assert_exists "${session_id}"

  db_session_lock_set "${session_id}" "${lock_value}"
}
