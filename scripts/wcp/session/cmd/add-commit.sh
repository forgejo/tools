# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_session_add_commit() {
  local session_id ref
  session_id="${1:-}"
  ref="${2:-}"

  if [ -z "${session_id}" ]; then
    echo "Error: Cannot add commits to a session without a session id." >&2
    exit 1
  fi

  assert_correct_branch "${session_id}"

  if [ -z "${ref}" ]; then
    echo "Error: Need a commit reference to add." >&2
    exit 1
  fi

  ref="$(git_ref_get "${ref}")"
  if [ -z "${ref}" ]; then
    echo "Error: Unable to resolve reference: '${2}'." >&2
    exit 1
  fi

  n="$(db_session_commits_count "${session_id}")"
  db_session_commit_add_new "${session_id}" "${n}" "${ref}"
}
