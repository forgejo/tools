# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_session_pr() {
  local session_id subcommand
  session_id="${1}"
  shift

  if [ -z "${1:-}" ]; then
    subcommand="help"
  else
    subcommand="${1}"
    shift
  fi

  case "${subcommand}" in
    help | h)
      cmd_wcp_session_pr_help
      ;;
    sync | s)
      cmd_wcp_session_pr_sync "${session_id}" "$@"
      ;;
    *)
      echo "Unknown subcommand: '${subcommand}'" >&2
      cmd_wcp_session_pr_help >&2
      exit 1
      ;;
  esac
}

cmd_wcp_session_pr_help() {
  cat <<EOF
wcp session pr <SUBCOMMAND> ...

All of these subcommands - apart from help - need configuration. The following
environment variables MUST be present and valid for these commands to work:

  - WCP_PULL_REQUEST_REMOTE=origin
  - WCP_FORGE=https://codeberg.org
  - WCP_BASE_REPO=forgejo/forgejo
  - WCP_FORGE_API_TOKEN=

Optionally, WCP_PULL_REQUEST_LABEL_IDS can be set to a comma separated list of
label ids to apply to the pull request on sync.

These can be set through the environment or through the configuration file
located at: '${CONFIG_FILE}'.

Subcommands:
  sync [--dry-run|-n]
  s    ...
    Sync the session to the ongoing pull request. If no pull request exists, open one.

    If dry-run mode is enabled, do not sync, nor open, just print what would be
    done otherwise.

  help
  h
    This help screen.
EOF
}

pr_assert_environment() {
  if [ -z "${WCP_PULL_REQUEST_REMOTE:-}" ]; then
    echo "Error: WCP_PULL_REQUEST_REMOTE unset! Please set it in ${CONFIG_FILE}, or export it." >&2
    exit 1
  fi

  if [ -z "${WCP_FORGE:-}" ]; then
    echo "Error: WCP_FORGE unset! Please set it in ${CONFIG_FILE}, or export it." >&2
    exit 1
  fi

  if [ -z "${WCP_BASE_REPO:-}" ]; then
    echo "Error: WCP_BASE_REPO unset! Please set it in ${CONFIG_FILE}, or export it." >&2
    exit 1
  fi

  if [ -z "${WCP_FORGE_API_TOKEN:-}" ]; then
    echo "Error: WCP_FORGE_API_TOKEN unset! Please set it in ${CONFIG_FILE}, or export it." >&2
    exit 1
  fi
}

cmd_wcp_session_pr_sync() {
  local session_id dry_run opt
  session_id="${1}"
  opt="${2:-}"
  dry_run=""

  if [ -z "${session_id}" ]; then
    echo "Error: Cannot open a pull request without a session-id." >&2
    exit 1
  fi
  session_assert_exists "${session_id}"

  assert_correct_branch "${session_id}"
  pr_assert_environment

  case "${opt}" in
    --dry-run | -n)
      dry_run="1"
      ;;
  esac

  local source_branch target_branch current_branch
  local week
  source_branch="$(db_session_source_branch_get "${session_id}")"
  target_branch="$(db_session_target_base_branch_get "${session_id}")"
  week="$(db_session_week_get "${session_id}")"
  current_branch="wcp/${week}"

  local pull_req
  pull_req=""

  if [ -z "${dry_run}" ]; then
    local outfile
    outfile="$(mktemp)"

    if ! git push "${WCP_PULL_REQUEST_REMOTE}" "${current_branch}:refs/for/${target_branch}/${current_branch}" \
      -o title="[gitea] week ${week} cherry pick (${source_branch} -> ${target_branch})" \
      -o description="" -o force-push=1 -f >"${outfile}" 2>&1; then
      if ! grep -q "\(The new commit is the same as the old commit\|new commit is same with old commit\)" "${outfile}"; then
        cat "${outfile}"
        exit 1
      fi
      pull_req="$(db_session_checklist_self_pr_get "${session_id}")"
    else
      pull_req="$(grep -A1 "^remote: Visit the.*pull request:" "${outfile}" | tail -n1 | sed -e "s,^.*/pulls/,," | tr -d ' ')"

      db_session_checklist_update_field "${session_id}" "self-pr" "${pull_req}"
    fi

    rm -f "${outfile}"
  else
    cat <<EOF
# Use the AGit workflow to open/update the pull request contents
git push "${WCP_PULL_REQUEST_REMOTE}" "${current_branch}:refs/for/${target_branch}/${current_branch}" \\
    -o title="[gitea] week ${week} cherry pick (${source_branch} -> ${target_branch})" \\
    -o description="" -o force-push=1 -f

EOF
  fi

  # Turn the statefile into JSON
  local payload
  payload="$(cmd_wcp_session_preview "${session_id}" | jq -R -s '. as $body | {body: $body}')"
  api_pr "${dry_run}" PATCH "pulls/${pull_req}" "${payload}" \
    "Update the pull request's comment via the API"

  # Apply labels
  if [ -n "${WCP_PULL_REQUEST_LABEL_IDS:-}" ]; then
    api_pr "${dry_run}" POST "issues/${pull_req}/labels" \
      "{\"labels\": [ ${WCP_PULL_REQUEST_LABEL_IDS} ]}" \
      "Add labels to the pull request"
  fi
}
