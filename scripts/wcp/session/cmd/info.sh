# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_session_info() {
  local session_id info week note target_base_branch target_base_ref source_branch source_start_ref source_end_ref locked
  session_id="${1}"

  if [ -z "${session_id}" ]; then
    echo "No sessions found." >&2
    exit 1
  fi

  if ! db_session_started "${session_id}"; then
    cat <<EOF
Session #${session_id} created, but not started yet.
EOF
    return
  fi

  # id | week | target-base-branch | target-base-ref | source_branch | source-start-ref | source-end-ref | locked
  IFS="|" read -r -a info <<<"$(db_session_get_info "${session_id}")"
  week="${info[1]}"
  target_base_branch="${info[2]}"
  target_base_ref="${info[3]}"
  source_branch="${info[4]}"
  source_start_ref="${info[5]}"
  source_end_ref="${info[6]}"
  case "${info[7]}" in
    0)
      locked="In progress"
      ;;
    1)
      locked="Locked"
      ;;
    *)
      echo "Database error: session.locked is not bool: '${info[7]}'" >&2
      exit 1
      ;;
  esac
  note="$(db_session_get_note "${session_id}")"

  cat <<EOF
Session #${session_id}, for week ${week}${note:+ (${note})}:
  - Targets '${target_base_branch}' (starting at '${target_base_ref}')
  - Picking from '${source_branch}' (${source_start_ref}...${source_end_ref})
  - ${locked}
  - Commits: $(db_session_commits_count "${session_id}")
EOF

  for mark in todo pick skip port in-progress; do
    echo "    - ${mark}: $(db_session_commits_count_marked "${session_id}" "${mark}")"
  done
}
