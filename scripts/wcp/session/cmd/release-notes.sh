# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_session_release_notes() {
  local session_id self_pr
  session_id="${1}"
  shift

  self_pr="$(db_session_checklist_self_pr_get "${session_id}")"
  if [ -z "${self_pr}" ] || [ "${self_pr}" = "true" ]; then
    echo "ERROR: There is no pull request for this session yet. Create one before writing release notes." >&2
    exit 1
  fi

  if [ ! -e "release-notes/${self_pr}.md" ]; then
    wcp_session_release_notes_init "${session_id}" "${self_pr}"
  fi

  ${EDITOR:-vim} "release-notes/${self_pr}.md"

  if grep -q "^TODO:" "release-notes/${self_pr}.md"; then
    echo "Please finalize the release notes and run this command again." >&2
    exit 1
  fi

  git add "release-notes/${self_pr}.md"

  local week
  week="$(db_session_week_get "${session_id}")"
  git commit -m "chore(release-notes): notes for the week ${week} weekly cherry pick"

  db_session_checklist_update_field "${session_id}" "release-notes" "true"
}

wcp_session_release_notes_init() {
  local session_id note_file
  session_id="${1}"
  note_file="release-notes/${2}.md"
  shift
  shift

  commits="$(db_session_commits_list_marked "${session_id}" "pick" | tac)"

  mkdir -p "release-notes"
  (for commit_id in ${commits}; do
    commit_render_for_release_notes "${session_id}" "${commit_id}"
  done) >"${note_file}"
}
