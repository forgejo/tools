# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_session_list_commits() {
  local session_id mark
  session_id="${1}"
  shift

  case "${1:-}" in
    --mark=*)
      mark="${1/--mark=/}"
      ;;
  esac

  if [ -z "${session_id}" ]; then
    echo "Error: Cannot list commits without a session id." >&2
    exit 1
  fi

  local commits
  if [ -z "${mark:-}" ]; then
    commits="$(db_session_commits_list "${session_id}")"
  else
    commits="$(db_session_commits_list_marked "${session_id}" "${mark}")"
  fi

  for commit_id in ${commits}; do
    echo "${commit_id} :$(db_commit_mark_get "${session_id}" "${commit_id}"): $(git_commit_subject_get "${commit_id}")"
  done
}
