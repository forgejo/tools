# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_session_checklist() {
  local session_id subcommand
  session_id="${1}"
  shift

  if [ -z "${1:-}" ]; then
    subcommand="view"
  else
    subcommand="${1}"
    shift
  fi

  case "${subcommand}" in
    help | h)
      cmd_wcp_session_checklist_help
      ;;
    view | v)
      cmd_wcp_session_checklist_view "${session_id}"
      ;;
    set | s)
      cmd_wcp_session_checklist_set "${session_id}" "$@"
      ;;
    *)
      echo "Unknown subcommand: '${subcommand}'" >&2
      cmd_wcp_session_checklist_help >&2
      exit 1
      ;;
  esac
}

cmd_wcp_session_checklist_help() {
  cat <<EOF
wcp session checklist <SUBCOMMAND> ...

Aliases:
  wcp session cl

Subcommands:
  view
  v
    View the current checklist.

  set <FIELD> <VALUE>
  s   ...
    Set the <FIELD> field of the checklist to <VALUE>.
    Available fields are:
      - previous-pick-pr
      - self-pr
      - ci-passed
      - e2e-label-pr, e2e-result-action-run
      - release-notes
      - reviewers-assigned
      - matrix-48h-call-url

  help
  h
    This help screen.
EOF
}

cmd_wcp_session_checklist_set() {
  local session_id field value
  session_id="${1}"
  field="${2:-}"
  value="${3:-}"

  if [ -z "${field}" ] || [ -z "${value}" ]; then
    echo "Error: session checklist set: Field ('${field}') or value ('${value}') is empty!" >&2
    exit 1
  fi

  db_session_checklist_update_field "${session_id}" "${field}" "${value}"
}

cmd_wcp_session_checklist_view() {
  local session_id info
  session_id="${1}"

  local previous_pick_pr
  local self_pr ci_passed
  local e2e_label_pr e2e_result_action_run
  local release_notes
  local reviewers_assigned
  local matrix_48h_call_url

  IFS="|" read -r -a info <<<"$(db_session_checklist_info_get "${session_id}")"
  previous_pick_pr="${info[1]}"
  self_pr="${info[2]}"
  ci_passed="${info[3]}"
  e2e_label_pr="${info[4]}"
  e2e_result_action_run="${info[5]}"
  reviewers_assigned="${info[6]}"
  matrix_48h_call_url="${info[7]:-}"
  release_notes="${info[8]:-}"

  echo -n "- [$(x "${previous_pick_pr}")] go to the last cherry-pick PR"
  if [ -n "${previous_pick_pr}" ]; then
    echo -n " (forgejo/forgejo#${previous_pick_pr})"
  fi
  echo -n " to figure out how far it went"

  local source_start_ref
  source_start_ref="$(db_session_source_start_ref_get "${session_id}")"
  if [ -n "${source_start_ref}" ]; then
    # shellcheck disable=SC2001
    short_id="$(echo "${source_start_ref}" | sed -e "s,^\(.\{10\}\).*,\1,")"
    echo ": [gitea@${short_id}]($(commit_url_gitea_commit "${source_start_ref}"))"
  else
    echo
  fi

  echo "- [$(x "${self_pr}")] cherry-pick and open PR${self_pr:+ (forgejo/forgejo#${self_pr})}"

  echo "- [$(x "${ci_passed}")] have the PR pass the CI"

  echo "- end-to-end (specially important if there are actions related changes)"
  echo -n "  - [$(x "${e2e_label_pr}")] add \`run-end-to-end\` label"
  if [ -n "${e2e_label_pr}" ]; then
    echo ": https://code.forgejo.org/forgejo/end-to-end/pulls/${e2e_label_pr}"
  else
    echo
  fi
  echo -n "  - [$(x "${e2e_result_action_run}")] check the result"
  if [ -n "${e2e_result_action_run}" ]; then
    echo ": https://code.forgejo.org/forgejo/end-to-end/actions/runs/${e2e_result_action_run}"
  else
    echo
  fi

  echo "- [$(x "${release_notes}")] write release notes"
  echo "- [$(x "${reviewers_assigned}")] assign reviewers"

  echo -n "- [$(x "${matrix_48h_call_url}")] 48h later, last call"
  if [ -n "${matrix_48h_call_url}" ]; then
    echo ": ${matrix_48h_call_url}"
  else
    echo
  fi

  echo "- merge 1 hour after the last call"
}
