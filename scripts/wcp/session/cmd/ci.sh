# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_session_ci() {
  local session_id subcommand
  session_id="${1}"
  shift

  if [ -z "${1:-}" ]; then
    subcommand="status"
  else
    subcommand="${1}"
    shift
  fi

  case "${subcommand}" in
    help | h)
      cmd_wcp_session_ci_help
      ;;
    status | s)
      cmd_wcp_session_ci_status "${session_id}"
      ;;
    log | l)
      cmd_wcp_session_ci_log "${session_id}" "$@"
      ;;
    sync)
      cmd_wcp_session_ci_sync "${session_id}" "$@"
      ;;
    *)
      echo "Unknown subcommand: '${subcommand}'" >&2
      cmd_wcp_session_ci_help >&2
      exit 1
      ;;
  esac
}

cmd_wcp_session_ci_help() {
  cat <<EOF
wcp session ci <SUBCOMMAND> ...

All of these subcommands - apart from help - need configuration. The following
environment variables MUST be present and valid for these commands to work:

  - WCP_FORGE=https://codeberg.org
  - WCP_BASE_REPO=forgejo/forgejo

These can be set through the environment or through the configuration file
located at: '${CONFIG_FILE}'.

Subcommands:
  status
  s
    If there's a pull request, display the CI statuses for it.

  log <N>
  l   ...
    Retrieve the Nth log (as listed by ci-status) for local inspection.

  sync [--dry-run | -n]
    Sync the pull request's CI status with the session's checklist.

  help
  h
    This help screen.
EOF
}

cmd_wcp_session_ci_status() {
  local session_id
  session_id="${1}"

  local pr
  pr="$(db_session_checklist_self_pr_get "${session_id}")"

  if [ -z "${pr}" ]; then
    echo "Error: No pull request set for the session yet!" >&2
    exit 1
  fi

  local statuses count
  statuses="$(api_log_list "${pr}")"
  count="$(echo "${statuses}" | jq length)"

  # shellcheck disable=SC2004
  for i in $(seq 0 $((${count} - 1))); do
    local status
    status="$(echo "${statuses}" | jq ".[$i]")"

    local desc result id
    desc="$(echo "${status}" | jq -r .context)"
    result="$(echo "${status}" | jq -r .status)"
    id="$(echo "${status}" | jq -r .target_url | sed -e "s,/${WCP_BASE_REPO}/actions/runs/\([0-9]*\)/jobs/\([0-9]*\),\1/\2,")"

    echo "${i} (${id}):|$desc|($result)"
  done | column -t -s '|'
}

cmd_wcp_session_ci_sync() {
  local session_id
  session_id="${1}"

  local pr
  pr="$(db_session_checklist_self_pr_get "${session_id}")"

  if [ -z "${pr}" ]; then
    echo "Error: No pull request set for the session yet!" >&2
    exit 1
  fi

  local statuses count
  statuses="$(api_log_list "${pr}")"
  count="$(echo "${statuses}" | jq length)"

  local ci_passed_old ci_passed_new
  ci_passed_old="$(db_session_checklist_get_field "${session_id}" "ci-passed")"
  ci_passed_new="true"

  # shellcheck disable=SC2004
  for i in $(seq 0 $((${count} - 1))); do
    local status result
    status="$(echo "${statuses}" | jq ".[$i]")"
    result="$(echo "${status}" | jq -r .status)"

    if [ "${result}" != "success" ]; then
      ci_passed_new="false"
    fi
  done

  db_session_checklist_update_field "${session_id}" "ci-passed" "${ci_passed_new}"
  if [ "${ci_passed_old}" != "${ci_passed_new}" ]; then
    cmd_wcp_session_pr_sync "$@"
  fi
}

cmd_wcp_session_ci_log() {
  local session_id n
  session_id="${1}"
  n="${2}"

  local pr
  pr="$(db_session_checklist_self_pr_get "${session_id}")"

  if [ -z "${pr}" ]; then
    echo "Error: No pull request set for the session yet!" >&2
    exit 1
  fi

  local statuses
  statuses="$(api_log_list "${pr}")"

  local status
  status="$(echo "${statuses}" | jq -c ".[${n}]")"

  if [ "${status}" = "null" ]; then
    echo "Unable to find logs for run #${n} of PR#${pr}." >&2
    exit 1
  fi

  local desc result url id
  desc="$(echo "${status}" | jq -r .context)"
  result="$(echo "${status}" | jq -r .status)"
  url="$(echo "${status}" | jq -r .target_url)"
  # shellcheck disable=SC2001
  id="$(echo "${url}" | sed -e "s,/${WCP_BASE_REPO}/actions/runs/\([0-9]*\)/jobs/\([0-9]*\),\1/\2,")"

  echo "# Logs for PR#${pr}, run #${n} (${id}): ${desc} (${result}) #" | sed -e 's,.,#,g'
  echo "# Logs for PR#${pr}, run #${n} (${id}): ${desc} (${result}) #"
  echo "# Logs for PR#${pr}, run #${n} (${id}): ${desc} (${result}) #" | sed -e 's,.,#,g'
  echo
  api_log_fetch "${url}"
}
