# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_session_add_commits() {
  local session_id until
  session_id="${1}"

  if [ -z "${session_id}" ]; then
    echo "Error: Cannot add commits to a session without a session id." >&2
    exit 1
  fi

  assert_correct_branch "${session_id}"

  case "${2:-}" in
    --until=*)
      until="$(git_ref_get "${2/--until=/}")"
      if [ -z "${until}" ]; then
        echo "Error: Unable to resolve reference: '${2/--until=/}'." >&2
        exit 1
      fi
      ;;
  esac

  if [ -z "${until:-}" ]; then
    until="$(git_ref_get "$(db_session_source_branch_get "${session_id}")")"
  fi

  local old_end_ref commits
  old_end_ref="$(db_session_source_end_ref_get "${session_id}")"
  commits="$(git_commit_list_range "${old_end_ref}" "${until}")"

  n="$(db_session_commits_count "${session_id}")"
  for commit_id in ${commits}; do
    db_session_commit_add_new "${session_id}" "${n}" "${commit_id}"
    n=$((n + 1))
  done

  db_session_source_end_ref_update "${session_id}" "${until}"
}
