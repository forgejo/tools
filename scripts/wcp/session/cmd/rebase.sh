# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_session_rebase() {
  local session_id onto
  session_id="${1}"

  if [ -z "${session_id}" ]; then
    echo "Error: Cannot rebase a session without a session id." >&2
    exit 1
  fi

  assert_correct_branch "${session_id}"

  case "${2:-}" in
    --onto=*)
      onto="$(git_ref_get "${2/--onto=/}")"
      if [ -z "${onto}" ]; then
        echo "Error: Unable to resolve reference: '${2/--onto=/}'." >&2
        exit 1
      fi
      ;;
  esac

  if [ -z "${onto:-}" ]; then
    onto="$(git_ref_get "$(db_session_target_base_branch_get "${session_id}")")"
  fi

  git rebase -q "${onto}"

  db_session_target_base_ref_update "${session_id}" "${onto}"
}
