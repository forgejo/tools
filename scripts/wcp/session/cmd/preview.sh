# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_session_preview() {
  local session_id opt
  session_id="${1}"
  opt="${2:-}"

  assert_correct_branch "${session_id}"

  if [ "${opt}" = "--dashboard" ]; then
    session_render_dashboard "${session_id}"
    return
  fi

  session_render_checklist "${session_id}"
  echo
  session_render_legend
  echo
  session_render_commits_handled "${session_id}"
  echo
  session_render_commits_todo "${session_id}"
  echo
  session_render_commits_skipped "${session_id}"
  echo
  session_render_stats "${session_id}"
}
