# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

. "${_libdir}/session/db/checklist.sh"
. "${_libdir}/session/db/commits.sh"
. "${_libdir}/session/db/session-getters.sh"

db_session_delete() {
  local session_id
  session_id="${1}"

  (
    cat <<EOF
BEGIN TRANSACTION;
DELETE FROM links WHERE "session-id"=${session_id};
DELETE FROM commits WHERE "session-id"=${session_id};
DELETE FROM checklist WHERE "session-id"=${session_id};
DELETE FROM session WHERE "id"=${session_id};
COMMIT TRANSACTION;
EOF
  ) | sqlite3 "${DBFILE}"
}

db_session_lock_set() {
  local session_id value
  session_id="${1}"
  value="${2}"

  sqlite3 "${DBFILE}" "UPDATE session SET locked=${value} WHERE id=${session_id};"
}

db_session_started() {
  local session_id count
  session_id="${1}"

  count="$(sqlite3 "${DBFILE}" "SELECT COUNT(id) FROM session WHERE id=${session_id} AND week NOT NULL;")"
  if [ "${count}" = "0" ]; then
    return 1
  else
    return 0
  fi
}

db_session_exists() {
  local session_id output
  session_id="${1}"

  output="$(sqlite3 "${DBFILE}" "SELECT '1' FROM session WHERE id=${session_id}")"
  [ -n "${output}" ]
}

db_session_update_params() {
  local session_id source target from notefile
  session_id="${1}"
  week="${2}"
  source="${3}"
  target="${4}"
  from="${5}"
  notefile="${6}"

  sqlite3 "${DBFILE}" "UPDATE session SET week='${week}', \"source-branch\"='${source}', \"target-base-branch\"='${target}', \"source-start-ref\"='${from}', note=cast(readfile('${notefile}') AS TEXT) WHERE id=${session_id};"
}

db_session_update_refs() {
  local session_id target_base_ref source_end_ref
  session_id="${1}"
  target_base_ref="${2}"
  source_end_ref="${3}"

  sqlite3 "${DBFILE}" "UPDATE session SET \"target-base-ref\"='${target_base_ref}', \"source-end-ref\"='${source_end_ref}' WHERE id=${session_id};"
}

db_session_target_base_ref_update() {
  local session_id target_base_ref
  session_id="${1}"
  target_base_ref="${2}"

  sqlite3 "${DBFILE}" "UPDATE session SET \"target-base-ref\"='${target_base_ref}' WHERE id=${session_id};"
}

db_session_source_end_ref_update() {
  local session_id source_end_ref
  session_id="${1}"
  source_end_ref="${2}"

  sqlite3 "${DBFILE}" "UPDATE session SET \"source-end-ref\"='${source_end_ref}' WHERE id=${session_id};"
}
