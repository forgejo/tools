# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

. "${_libdir}/session/helpers/render.sh"

session_default_week() {
  echo "$(date +%Y)-$((10#$(date +%U) + 1))"
}

session_assert_exists() {
  local session_id
  session_id="${1}"

  if ! db_session_exists "${session_id}"; then
    echo "Error: session does not exist: '${session_id}'." >&2
    exit 1
  fi
}
