# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

git_commit_list_range() {
  local start end
  start="${1}"
  end="${2}"

  git log --reverse --format=format:%H --no-decorate "${start}...${end}"
}

git_commit_subject_get() {
  local commit_id
  commit_id="${1}"

  git --no-pager show --format=format:%s -s "${commit_id}"
}

git_commit_find_cherry_pick() {
  local commit_id
  commit_id="${1}"

  git log --format=format:%H -n 1 -q --grep "cherry picked from commit ${commit_id}"
}

git_modified_files() {
  local commit_id
  commit_id="${1}"

  git show "${commit_id}" --numstat --format=format: | sed -e "s,^[0-9]*[\s\t]*[0-9]*[\s\t]*,,"
}
