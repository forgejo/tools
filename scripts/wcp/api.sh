# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

api_whoami() {
  curl -sS --fail-with-body "${WCP_FORGE}/api/v1/user" \
    -H "accept: application/json" \
    -H "authorization: Bearer ${WCP_FORGE_API_TOKEN}" |
    jq -r ".login"
}

api_log_list() {
  local pr
  pr="${1}"

  local commit_id
  commit_id="$(curl -s "${WCP_FORGE}/api/v1/repos/${WCP_BASE_REPO}/pulls/${pr}/commits?limit=1" \
    -H "accept: application/json" |
    jq -r ".[0].sha")"

  curl -s "${WCP_FORGE}/api/v1/repos/${WCP_BASE_REPO}/commits/${commit_id}/statuses" |
    jq -c ".[] |= {status, target_url, context}" |
    jq -c 'reduce .[] as $i ({}; if . | has($i.target_url) | not then .[$i.target_url] = $i end) | to_entries | map_values(.value) | sort_by(.target_url)'
}

api_log_fetch() {
  local job_url
  job_url="${1}"

  curl -s "${WCP_FORGE}${job_url}/logs"
}

api_pr() {
  local dry_run method api_path data comment
  dry_run="${1}"
  method="${2}"
  api_path="${3}"
  data="${4}"
  comment="${5}"

  local output_stdout output_stderr script retval
  output_stdout="$(mktemp)"
  output_stderr="$(mktemp)"
  script="$(mktemp)"
  retval=0

  secret() {
    if [ -n "${dry_run}" ]; then
      # shellcheck disable=SC2016
      echo '${WCP_FORGE_API_TOKEN}'
    else
      echo "$@"
    fi
  }

  placeholder() {
    # shellcheck disable=SC2001,SC2016
    echo "$@" | sed -e 's,\(/$\|/\(/\)\),/${WCP_PULL_REQUEST_ID}\2,'
  }

  # shellcheck disable=SC2001,SC2116
  cat >"${script}" <<EOF
##$(echo "${comment}" | sed -e 's,.,#,g')##
# ${comment} #
##$(echo "${comment}" | sed -e 's,.,#,g')##

# Prepare the payload
payload="\$(mktemp)"
cat >"\${payload}" <<'PAYLOAD'
$(echo "${data}")
PAYLOAD

curl -sS --fail-with-body -X ${method} \\
     --header "content-type: application/json" \\
     --header "authorization: Bearer $(secret "${WCP_FORGE_API_TOKEN}")" \\
     --data "@\${payload}" \\
     "${WCP_FORGE}/api/v1/repos/${WCP_BASE_REPO}/$(placeholder "${api_path}")"

rm -f "\${payload}"
EOF

  if [ -n "${dry_run}" ]; then
    cat "${script}"
    echo
    return
  fi
  if ! bash "${script}" >"${output_stdout}" 2>"${output_stderr}"; then
    (
      echo "Failed to ${method} ${WCP_FORGE}/api/v1/repos/${WCP_BASE_REPO}/$(placeholder "${api_path}")"
      cat "${output_stdout}"
      cat "${output_stderr}"
    ) >&2
    retval=1
  fi

  rm -f "${output_stdout}" "${output_stderr}" "${script}"
  return "${retval}"
}
