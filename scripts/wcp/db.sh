# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

. "${_libdir}/db/cmd.sh"

DB_CURRENT_VERSION=1

db_init() {
  db_init_v0 | sqlite3 "${DBFILE}"
  for v in $(seq 1 "${DB_CURRENT_VERSION}"); do
    db_migrate "${v}"
  done
}

db_init_v0() {
  cat <<EOF
CREATE TABLE IF NOT EXISTS 'session' (
  'id'                 INTEGER PRIMARY KEY ASC,
  'week'               INTEGER UNIQUE,
  'note'               TEXT,
  'target-base-branch' TEXT,
  'target-base-ref'    TEXT,
  'source-branch'      TEXT,
  'source-start-ref'   TEXT,
  'source-end-ref'     TEXT,
  'locked'             BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE IF NOT EXISTS 'commits' (
  'session-id'       INTEGER NOT NULL,
  'source-commit-id' TEXT NOT NULL,
  'n'                INTEGER NOT NULL,
  'mark'             TEXT NOT NULL DEFAULT 'todo',
  'comments'         TEXT
);
CREATE UNIQUE INDEX IF NOT EXISTS 'session-commits' ON 'commits' ('session-id', 'source-commit-id');

CREATE TABLE IF NOT EXISTS 'checklist' (
  'session-id'                INTEGER NOT NULL,
  'interest-check'            BOOLEAN NOT NULL DEFAULT false,
  'previous-pick-pr'          INTEGER,
  'self-pr'                   INTEGER,
  'ci-passed'                 BOOLEAN NOT NULL DEFAULT false,
  'e2e-label-pr'              INTEGER,
  'e2e-result-action-run'     INTEGER,
  'reviewers-assigned'        BOOLEAN NOT NULL DEFAULT false,
  'matrix-48h-call-url'       TEXT
);
CREATE UNIQUE INDEX IF NOT EXISTS 'checklist-session' ON 'checklist' ('session-id');

CREATE TABLE IF NOT EXISTS 'links' (
  'session-id' INTEGER NOT NULL,
  'parent-id'  INTEGER NOT NULL,
  'child-id'   INTEGER NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS 'links-rel' ON 'links' ('session-id', 'parent-id', 'child-id');

CREATE TABLE IF NOT EXISTS 'versions' (
  'applied' INTEGER UNIQUE NOT NULL DEFAULT 0
);
EOF
}

db_version_is_applied() {
  local version
  version="${1}"

  sqlite3 "${DBFILE}" "SELECT 1 FROM versions WHERE \"applied\" = ${version}"
}

db_migrate() {
  local version
  version="${1}"

  if [ -z "$(db_version_is_applied "${version}")" ]; then
    "db_migrate_v${version}"
  fi
}

db_migrate_v1() {
  # The v1 migration is special, because the "release-notes" column *may*
  # already exist, and if it does, we shouldn't fail. So lets alter the table,
  # quietly, allowing it to fail.
  sqlite3 "${DBFILE}" 'ALTER TABLE checklist ADD COLUMN "release-notes" BOOLEAN NOT NULL DEFAULT false;' 2>/dev/null || true

  # ..and then add ourselves as an applied version
  sqlite3 "${DBFILE}" "INSERT INTO versions (applied) VALUES (1);"
}
