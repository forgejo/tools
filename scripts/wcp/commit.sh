# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

. "${_libdir}/commit/db.sh"
. "${_libdir}/commit/helpers.sh"
. "${_libdir}/commit/cmd.sh"
