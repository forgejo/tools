# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

STATE_DIR="${XDG_STATE_HOME:-${HOME:-~}/.local/state}/forgejo-wcp"
CONFIG_DIR="${XDG_CONFIG_HOME:-${HOME:-~}/.config}/forgejo-wcp"
CONFIG_FILE="${CONFIG_DIR}/config.sh"
DBFILE="${STATE_DIR}/db.sqlite"

WCP_FORGE="${WCP_FORGE:-https://codeberg.org}"
WCP_BASE_REPO="${WCP_BASE_REPO:-forgejo/forgejo}"
WCP_PULL_REQUEST_REMOTE="${WCP_PULL_REQUEST_REMOTE:-origin}"

gitea_specific_patterns="options/locale/
\.github/
\.gitea/
docs/
CHANGELOG\.md
"

. "${_libdir}/db.sh"
. "${_libdir}/git.sh"
. "${_libdir}/help.sh"
. "${_libdir}/session.sh"
. "${_libdir}/commit.sh"
. "${_libdir}/api.sh"

x() {
  local v
  v="${1}"

  if [ -z "${v}" ] || [ "${v}" = "0" ]; then
    echo " "
  else
    echo "x"
  fi
}
