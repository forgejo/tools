# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

. "${_libdir}/commit/helpers/urls.sh"

db_list_portables() {
  sqlite3 "${DBFILE}" \
    "SELECT session.week, session.id, checklist.\"self-pr\", commits.\"source-commit-id\"
     FROM commits
     INNER JOIN session ON session.id = commits.\"session-id\"
     INNER JOIN checklist ON session.id = checklist.\"session-id\"
     WHERE commits.mark = 'port'
     ORDER BY session.week ASC, commits.n ASC"
}

cmd_wcp_db_list_portables() {
  local curr_week
  curr_week=""

  for commit in $(db_list_portables); do
    local week session_id self_pr gitea_commit_id forgejo_commit

    # week | session_id | self-pr | commit-id
    IFS="|" read -r -a info <<<"${commit}"
    week="${info[0]}"
    session_id="${info[1]}"
    self_pr="${info[2]}"
    gitea_commit_id="${info[3]}"

    # If starting a new week, display a header
    if [ "${week}" != "${curr_week}" ]; then
      if [ -n "${curr_week}" ]; then
        echo
      fi
      curr_week="${week}"
      echo "## [Week ${week}]($(commit_url_forgejo_pr "${self_pr}"))"
    fi

    # Check if we have cherry-picked/ported the commit meanwhile
    forgejo_commit="$(git_commit_find_cherry_pick "${gitea_commit_id}")"
    if [ -z "${forgejo_commit}" ]; then
      # ...if not, display it
      local pr message

      pr="$(commit_pr_find "${gitea_commit_id}")"
      message="$(commit_message_get "${gitea_commit_id}")"

      echo "[\`gitea#${pr}\`]($(commit_url_gitea_pr "${pr}")): ${message}"
    fi
  done
}
