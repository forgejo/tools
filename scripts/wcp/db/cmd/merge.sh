# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_db_merge() {
  local username dry_run
  username=""
  dry_run=""

  for opt in "$@"; do
    case "${opt}" in
      --dry-run | -n)
        dry_run="1"
        ;;
      *)
        username="${opt}"
        ;;
    esac
  done

  if [ -z "${username}" ]; then
    username="$(api_whoami)"
  fi

  local output_stdout output_stderr script retval
  output_stdout="$(mktemp)"
  output_stderr="$(mktemp)"
  script="$(mktemp)"
  retval=0

  secret() {
    if [ -n "${dry_run}" ]; then
      # shellcheck disable=SC2016
      echo '${WCP_FORGE_API_TOKEN}'
    else
      echo "$@"
    fi
  }

  cat >"${script}" <<EOF
set -euo pipefail

tmp_db="\$(mktemp)"

# Download the database.
curl -sS --fail-with-body \\
     "${WCP_FORGE}/api/packages/${username}/generic/wcp-database/current/db.sql" | \\
  sqlite3 "\${tmp_db}"
sqlite3 "${DBFILE}" <<SQL
attach '\${tmp_db}' AS merge;
BEGIN;
INSERT OR IGNORE INTO commits SELECT * FROM merge.commits;
INSERT OR IGNORE INTO links SELECT * FROM merge.links;
INSERT OR IGNORE INTO checklist SELECT * FROM merge.checklist;
INSERT OR IGNORE INTO session SELECT * FROM merge.session;
COMMIT;
detach merge;
SQL
EOF

  if [ -n "${dry_run}" ]; then
    cat "${script}"
    echo
    return
  fi

  if ! bash "${script}" >"${output_stdout}" 2>"${output_stderr}"; then
    (
      echo "Failed to pull the database dump from ${WCP_FORGE}/api/packages/${username}/generic/wcp-database/current/db.sql"
      cat "${output_stdout}"
      cat "${output_stderr}"
    ) >&2
    retval=1
  fi

  rm -f "${output_stdout}" "${output_stderr}" "${script}"
  return "${retval}"
}
