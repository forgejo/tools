# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

. "${_libdir}/db/cmd/push.sh"
. "${_libdir}/db/cmd/pull.sh"
. "${_libdir}/db/cmd/merge.sh"
. "${_libdir}/db/cmd/list-portables.sh"

cmd_wcp_db() {
  local subcmd
  subcmd="${1:-help}"
  shift || true

  case "${subcmd}" in
    export | e)
      cmd_wcp_db_export
      ;;
    import | i)
      cmd_wcp_db_import "$@"
      ;;
    push)
      cmd_wcp_db_push "$@"
      ;;
    pull)
      cmd_wcp_db_pull "$@"
      ;;
    merge)
      cmd_wcp_db_merge "$@"
      ;;
    list-portables)
      cmd_wcp_db_list_portables "$@"
      ;;
    help | h)
      cmd_wcp_db_help
      ;;
    *)
      echo "Unknown subcommand: '${subcmd}'" >&2
      cmd_wcp_db_help >&2
      exit 1
      ;;
  esac
}

cmd_wcp_db_help() {
  cat <<EOF
wcp db <SUBCOMMAND> ...

Subcommands:
  export
  e
    Export the current database as SQL, to standard output.

  import
  i
    Import a previously exported database, from standard input.
    Replaces the existing database on success.

  push [--dry-run|-n] [<USERNAME>] | pull [--dry-run|-n] [<USERNAME>] | merge [--dry-run|-n] [<USERNAME>]
    Push, pull or merge a database from ${WCP_FORGE}.
    When pulling, replaces the existing database on success.
    Merge will insert any new items from the pulled database into the current one.

    If no <USERNAME> is specified, defaults to the owner of the API token.

  list-portables
    List all the commits and PRs marked for porting.
EOF
}

cmd_wcp_db_export() {
  sqlite3 "${DBFILE}" ".dump"
}

cmd_wcp_db_import() {
  local tmp_db

  tmp_db="$(mktemp)"
  cat | sqlite3 "${tmp_db}"

  mv -f "${tmp_db}" "${DBFILE}"
}
