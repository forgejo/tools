# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_help() {
  local subcommand
  subcommand="${1:-}"

  if [ -n "${subcommand}" ]; then
    "cmd_wcp_${subcommand}_help"
    return
  fi

  cat <<EOF
wcp COMMAND ...

Commands:
  session ... | s ...
    Start, delete, preview cherry pick sessions.

  commit ...  | c ... | ...
    Handle individual commits within a session.

  notes help | n help
    Display some helpful notes

  db ...
    Database export/import. Use at your own risk.

  help [<SUBCOMMAND>] | h [<SUBCOMMAND>]
    Display usage information. If <SUBCOMMAND> is specified, display the help screen of that.
EOF
}

cmd_wcp_notes_help() {
  cat <<EOF
## Notes

- Use the weekly-cherry-pick.sh script at https://codeberg.org/forgejo/tools
- When cherry-picking
    - try to understand and resolve all conflicts to the extent that they are trivial
    - if they are not trivial abort the cherry-pick, it is better to schedule a port with the person who was last active in the same area
- Conflict resolution
  - All changes in \`docs/\` are silently resolved in favor of the commit
  - All \`options/locale\` changes are resolved in favor of Forgejo
  - The \`Conflicts\` section of the commit must contain detailed information about how the conflicts where resolved
- All Gitea database migrations are imported unmodified even if they are part of a commit that is skipped
- Test fail resolution
  - If they require Forgejo specific change or non trivial modifications of Gitea files, do them in a commit with the same title and a suffix (followup, part 2, etc.) and move the commit next to the one they relate to
  - If they require changing the Gitea commit in a trivial way amend the corresponding commit and make sure the comment the "Conflict" section of the commit message
- Skip commits and mark them "- gitea specific" if they only change
    - \`docs/\`
    - \`.github/\`
    - \`.gitea/\`
    - translations (commit title contains crowdin)
- Run deadcode and add the change in a \`[DEADCODE] update\` commit
  \`go run golang.org/x/tools/internal/cmd/deadcode@v0.14.0 -generated=false -test code.gitea.io/gitea > .deadcode-out\`
- To speed up the debug loop, run lint & tests locally to verify there are no hidden / non syntactic conflicts
- When a commit is \`skip\` or \`port\`, the comment that goes with it should:
  - tag the person who is most familiar with this area of the codebase
    - so they are notified in case they do not already watch the PR
    - to record that in case their expertise is needed later on
  - include the title of a related Forgejo commit when
    - there is a conflict
    - it is already implemented
EOF
}

cmd_wcp_help_help() {
  cmd_wcp_help ""
}
