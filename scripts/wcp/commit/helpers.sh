# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

. "${_libdir}/commit/helpers/urls.sh"
. "${_libdir}/commit/helpers/resolve.sh"
. "${_libdir}/commit/helpers/render.sh"

commit_pr_find() {
  local commit_id
  commit_id="${1}"

  git_commit_subject_get "${commit_id}" | sed -ne 's/.*(#\([0-9]*\))$/\1/p'
}

commit_message_get() {
  local commit_id
  commit_id="${1}"

  git_commit_subject_get "${commit_id}" | sed -e "s, *(#[0-9]*)$,,"
}

commit_browser_open_for_gitea_pr() {
  local commit_id
  commit_id="${1}"

  xdg-open "$(commit_url_gitea_pr_or_commit "${commit_id}")"
}

commit_has_parents() {
  local session_id commit_id
  session_id="${1}"
  commit_id="${2}"

  [ -n "$(db_commit_link_list_parents "${session_id}" "${commit_id}")" ]
}

assert_no_cherry_pick_in_progress() {
  if git_cherry_pick_in_progress; then
    echo "Error: A cherry pick is already in progress, finish that first!" >&2
    exit 1
  fi
}
