# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

. "${_libdir}/commit/db/ids.sh"
. "${_libdir}/commit/db/n.sh"
. "${_libdir}/commit/db/comments.sh"
. "${_libdir}/commit/db/mark.sh"
. "${_libdir}/commit/db/links.sh"
