# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

db_commit_link_add() {
  local session_id commit_id linked_id
  session_id="${1}"
  commit_id="${2}"
  linked_id="${3}"

  (
    cat <<EOF
INSERT OR REPLACE INTO links ("session-id", "parent-id", "child-id") VALUES (${session_id}, '${commit_id}', '${linked_id}');
EOF
  ) | sqlite3 "${DBFILE}"
}

db_commit_link_remove() {
  local session_id commit_id linked_id
  session_id="${1}"
  commit_id="${2}"
  linked_id="${3}"

  (
    cat <<EOF
DELETE FROM links WHERE "session-id"=${session_id} AND "parent-id"='${commit_id}' AND "child-id"='${linked_id}';
EOF
  ) | sqlite3 "${DBFILE}"
}

db_commit_link_list_children() {
  local session_id commit_id
  session_id="${1}"
  commit_id="${2}"

  (
    cat <<EOF
SELECT "child-id" FROM links WHERE "session-id"=${session_id} AND "parent-id"='${commit_id}' ORDER BY rowid ASC
EOF
  ) | sqlite3 "${DBFILE}"
}

db_commit_link_list_parents() {
  local session_id linked_id
  session_id="${1}"
  linked_id="${2}"

  (
    cat <<EOF
SELECT "parent-id" FROM links WHERE "session-id"=${session_id} AND "child-id"='${linked_id}'
EOF
  ) | sqlite3 "${DBFILE}"
}
