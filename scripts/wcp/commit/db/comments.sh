# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

db_commit_comments_get() {
  local session_id commit_id
  session_id="${1}"
  commit_id="${2}"

  sqlite3 "${DBFILE}" "SELECT comments FROM commits WHERE \"session-id\"=${session_id} AND \"source-commit-id\"='${commit_id}';"
}

db_commit_comments_update() {
  local session_id commit_id tmpfile
  session_id="${1}"
  commit_id="${2}"
  tmpfile="${3}"

  sqlite3 "${DBFILE}" "UPDATE commits SET comments=cast(readfile('${tmpfile}') AS TEXT) WHERE \"session-id\"=${session_id} AND \"source-commit-id\"='${commit_id}';"
}
