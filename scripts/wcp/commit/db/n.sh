# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

db_commit_get_n_for_first_pending() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT \"n\" FROM commits WHERE \"session-id\"=${session_id} AND mark='todo' ORDER BY n ASC LIMIT 1;"
}

db_commit_get_n() {
  local session_id commit_id
  session_id="${1}"
  commit_id="${2}"

  sqlite3 "${DBFILE}" "SELECT n FROM commits WHERE \"session-id\"=${session_id} AND \"source-commit-id\"='${commit_id}';"
}
