# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

db_commit_mark_update() {
  local session_id commit_id mark
  session_id="${1}"
  commit_id="${2}"
  mark="${3}"

  sqlite3 "${DBFILE}" "UPDATE commits SET mark='${mark}' WHERE \"session-id\"=${session_id} AND \"source-commit-id\"='${commit_id}';"
}

db_commit_mark_get() {
  local session_id commit_id
  session_id="${1}"
  commit_id="${2}"

  sqlite3 "${DBFILE}" "SELECT mark FROM commits WHERE \"session-id\"=${session_id} AND \"source-commit-id\"='${commit_id}';"
}
