# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

db_commit_get_id_for_first_pending() {
  local session_id
  session_id="${1}"

  sqlite3 "${DBFILE}" "SELECT \"source-commit-id\" FROM commits WHERE \"session-id\"=${session_id} AND mark='todo' ORDER BY n ASC LIMIT 1;"
}

db_commit_get_id_for_nth() {
  local session_id n
  session_id="${1}"
  n="${2}"

  sqlite3 "${DBFILE}" "SELECT \"source-commit-id\" FROM commits WHERE \"session-id\"=${session_id} AND n=${n};"
}

db_commit_get_full_id() {
  local session_id commit_prefix
  session_id="${1}"
  commit_prefix="${2}"

  sqlite3 "${DBFILE}" "SELECT \"source-commit-id\" FROM commits WHERE \"session-id\"=${session_id} AND \"source-commit-id\" LIKE '${commit_prefix}%' ORDER BY n ASC LIMIT 1;"
}
