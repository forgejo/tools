# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_commit_mark() {
  local session_id commit_id mark
  session_id="${1}"
  commit_id="${2}"
  mark="${3}"

  assert_no_cherry_pick_in_progress

  db_commit_mark_update "${session_id}" "${commit_id}" "${mark}"
  cmd_wcp_commit_edit "${session_id}" "${commit_id}"
}
