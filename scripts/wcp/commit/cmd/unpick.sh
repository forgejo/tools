# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_commit_unpick() {
  local session_id commit_id forgejo_commit_id
  session_id="${1}"
  commit_id="${2}"

  assert_correct_branch "${session_id}"
  assert_no_cherry_pick_in_progress

  forgejo_commit_id="$(git_commit_find_cherry_pick "${commit_id}")"

  if [ -z "${forgejo_commit_id}" ]; then
    echo "Error: Cannot unpick ${commit_id}, it doesn't appear in the git history." >&2
    exit 1
  fi

  included_commits="$(db_session_commits_list_marked "${session_id}" "pick")"

  tmpfile="$(mktemp)"
  echo "#! /bin/sh" >"${tmpfile}"
  # shellcheck disable=SC2016,SC2129
  echo 'cat >$1 <<EOF' >>"${tmpfile}"
  (for cid in ${included_commits}; do
    fcid="$(git_commit_find_cherry_pick "${cid}")"
    if [ "${fcid}" = "${forgejo_commit_id}" ]; then
      echo "drop ${fcid}"
    else
      echo "pick ${fcid}"
    fi
  done) >>"${tmpfile}"
  echo "EOF" >>"${tmpfile}"
  GIT_EDITOR="sh ${tmpfile}" git rebase -i "$(db_session_target_base_branch_get "${session_id}")"
  rm -f "${tmpfile}"

  db_commit_mark_update "${session_id}" "${commit_id}" "todo"
}
