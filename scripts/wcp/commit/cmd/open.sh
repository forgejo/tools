# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_commit_open() {
  local commit_id
  commit_id="${1}"

  commit_browser_open_for_gitea_pr "${commit_id}"
}
