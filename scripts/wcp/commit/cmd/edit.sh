# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_commit_edit() {
  local session_id commit_id
  session_id="${1}"
  commit_id="${2}"

  local comments
  comments="$(db_commit_comments_get "${session_id}" "${commit_id}")"
  tmpfile="$(mktemp --suffix=.md)"
  # shellcheck disable=SC2129
  commit_view_preamble "${session_id}" "${commit_id}" | sed -e "s,^,# ," >>"${tmpfile}"
  cat >>"${tmpfile}" <<EOF

${comments}

# NOTE: Lines beginning with a hashmark are comments, and will be ignored!
#
EOF

  commit_view_postamble "${session_id}" "${commit_id}" >>"${tmpfile}"

  if [ "${EDITOR:-}" = "cat" ]; then
    cat "${tmpfile}"
  else
    ${EDITOR:-vim} +4 "${tmpfile}"
  fi

  # Remove any comment lines
  comments="$(grep -v '^#' "${tmpfile}" || true)"
  # Remove the first line if its empty
  if [ -z "$(echo "${comments}" | head -n 1)" ]; then
    comments="$(echo "${comments}" | tail -n +2)"
  fi
  # Remove the last line if its empty
  if [ -z "$(echo "${comments}" | tail -n 1)" ]; then
    comments="$(echo "${comments}" | head -n -1)"
  fi
  echo "${comments}" >"${tmpfile}"

  db_commit_comments_update "${session_id}" "${commit_id}" "${tmpfile}"
  rm -f "${tmpfile}"
}
