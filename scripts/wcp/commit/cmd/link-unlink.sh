# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

resolve_commit_ref() {
  local session_id commit_ref
  session_id="${1}"
  commit_ref="${2}"

  case "${commit_ref}" in
    +* | -* | @*)
      commit_resolve_n "${session_id}" "${commit_ref}"
      ;;
    *)
      commit_resolve_id "${session_id}" "${commit_ref}"
      ;;
  esac
}

cmd_wcp_commit_link() {
  local session_id commit_id target
  session_id="${1}"
  commit_id="${2}"
  target="${3:-}"

  if [ -z "${target}" ]; then
    echo "Error: Need a target to link the selected commit to." >&2
    exit 1
  fi

  target="$(resolve_commit_ref "${session_id}" "${target}")"

  db_commit_link_add "${session_id}" "${target}" "${commit_id}"
}

cmd_wcp_commit_unlink() {
  local session_id commit_id target
  session_id="${1}"
  commit_id="${2}"
  target="${3:-}"

  if [ -z "${target}" ]; then
    echo "Error: Need a target to unlink the selected commit from." >&2
    exit 1
  fi

  target="$(resolve_commit_ref "${session_id}" "${target}")"

  db_commit_link_remove "${session_id}" "${target}" "${commit_id}"
}
