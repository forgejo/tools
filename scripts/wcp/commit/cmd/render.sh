# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_commit_render() {
  local session_id commit_id
  session_id="${1}"
  commit_id="${2}"

  commit_render "${session_id}" "${commit_id}"
}
