# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_commit_skip() {
  local session_id commit_id opt
  session_id="${1}"
  commit_id="${2}"
  opt="${3:-}"

  assert_correct_branch "${session_id}"
  assert_no_cherry_pick_in_progress

  # Check if the skipped commits contain Gitea-specific files only
  local gitea_specific
  modified_files="$(git_modified_files "${commit_id}")"

  for p in ${gitea_specific_patterns}; do
    modified_files="$(echo "${modified_files}" | grep -v "^${p}" || true)"
  done
  if [ -z "${modified_files}" ]; then
    gitea_specific=1
  else
    gitea_specific=0
  fi

  db_commit_mark_update "${session_id}" "${commit_id}" "skip"

  local reason edit
  case "${opt}" in
    --reason=*)
      reason="${opt/--reason=/}"
      edit=false
      ;;
    *)
      reason=""
      edit=true
      ;;
  esac

  if [ -z "${reason}" ] && [ "${gitea_specific}" = 1 ]; then
    reason="gitea"
  fi

  if [ -n "${reason}" ]; then
    tmpfile="$(mktemp)"

    db_commit_comments_get "${session_id}" "${commit_id}" >"${tmpfile}"

    case "${reason}" in
      gitea*)
        echo "- ${reason} specific" >>"${tmpfile}"
        edit=false
        ;;
      forgejo*)
        echo "- Already in Forgejo: ${reason}" >>"${tmpfile}"
        ;;
    esac

    db_commit_comments_update "${session_id}" "${commit_id}" "${tmpfile}"
    rm -f "${tmpfile}"
  fi

  if $edit; then
    cmd_wcp_commit_edit "${session_id}" "${commit_id}"
  fi
}
