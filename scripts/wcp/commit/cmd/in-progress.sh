# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_commit_in_progress() {
  local session_id commit_id opt
  session_id="${1}"
  commit_id="${2}"
  opt="${3:-}"

  assert_correct_branch "${session_id}"
  assert_no_cherry_pick_in_progress

  db_commit_mark_update "${session_id}" "${commit_id}" "in-progress"

  local pr
  case "${opt}" in
    --pr=*)
      pr="${opt/--pr=/}"
      ;;
    *)
      pr=""
      ;;
  esac

  if [ -n "${pr}" ]; then
    tmpfile="$(mktemp)"
    echo "- Ported at forgejo/forgejo#${pr}" >"${tmpfile}"
    db_commit_comments_update "${session_id}" "${commit_id}" "${tmpfile}"
    rm -f "${tmpfile}"
  fi

  cmd_wcp_commit_edit "${session_id}" "${commit_id}"
}
