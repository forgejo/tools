# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_commit_info() {
  local session_id commit_id
  session_id="${1}"
  commit_id="${2}"

  local comments
  comments="$(db_commit_comments_get "${session_id}" "${commit_id}")"

  commit_view_preamble "${session_id}" "${commit_id}"
  # shellcheck disable=SC2001
  echo "${comments}" | sed -e "s,^,  ,"
  echo
  commit_view_postamble "${session_id}" "${commit_id}"
}
