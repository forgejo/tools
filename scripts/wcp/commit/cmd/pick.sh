# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

cmd_wcp_commit_pick() {
  local session_id commit_id edit
  session_id="${1}"
  commit_id="${2}"
  edit="${3:-}"

  assert_correct_branch "${session_id}"
  assert_no_cherry_pick_in_progress

  if [ -z "$(git_commit_find_cherry_pick "${commit_id}")" ]; then
    if ! git_cherry_pick "${commit_id}"; then
      echo "Resolve the conflict, and run \`wcp commit pick\` again!" >&2
      exit 1
    fi
  fi

  db_commit_mark_update "${session_id}" "${commit_id}" "pick"
  if [ -n "${edit}" ] && [ -z "${edit/--edit/}" ]; then
    cmd_wcp_commit_edit "${session_id}" "${commit_id}"
  fi
}
