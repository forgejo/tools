# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

commit_url_forgejo_commit() {
  local commit_id
  commit_id="${1}"

  echo "https://codeberg.org/forgejo/forgejo/commit/${commit_id}"
}

commit_url_forgejo_pr() {
  local pr
  pr="${1}"

  echo "https://codeberg.org/forgejo/forgejo/pulls/${pr}"
}

commit_url_gitea_pr() {
  local pr
  pr="${1}"

  echo "https://github.com/go-gitea/gitea/pull/${pr}"
}

commit_url_gitea_commit() {
  local commit_id
  commit_id="${1}"

  echo "https://github.com/go-gitea/gitea/commit/${commit_id}"
}

commit_url_gitea_pr_or_commit() {
  local commit_id
  commit_id="$1"

  pr="$(commit_pr_find "${commit_id}")"

  if [ -z "${pr}" ]; then
    commit_url_gitea_commit "${commit_id}"
  else
    commit_url_gitea_pr "${pr}"
  fi
}
