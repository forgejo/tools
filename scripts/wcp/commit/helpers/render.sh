# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2
commit_view_preamble() {
  local session_id commit_id
  session_id="${1}"
  commit_id="${2}"

  assert_correct_branch "${session_id}"

  local n mark comments forgejo_commit
  n="$(db_commit_get_n "${session_id}" "${commit_id}")"
  mark="$(db_commit_mark_get "${session_id}" "${commit_id}")"
  pr="$(commit_pr_find "${commit_id}")"
  message="$(commit_message_get "${commit_id}")"

  if [ "${mark}" = "pick" ]; then
    forgejo_commit="$(git_commit_find_cherry_pick "${commit_id}")"
  else
    forgejo_commit=""
  fi

  cat <<EOF
# pick#${n} (gitea@${commit_id}${forgejo_commit:+ -> forgejo@${forgejo_commit}})
- :${mark}: ${message} ${pr:+(gitea#${pr})}
EOF
}

commit_view_children() {
  local session_id commit_id
  session_id="${1}"
  commit_id="${2}"

  assert_correct_branch "${session_id}"

  local children
  children="$(db_commit_link_list_children "${session_id}" "${commit_id}")"

  if [ -z "${children}" ]; then
    return
  fi

  echo "# Linked commits:"

  for child_id in ${children}; do
    echo "#  - ${child_id}"
  done
}

commit_view_postamble() {
  local session_id commit_id
  session_id="${1}"
  commit_id="${2}"

  assert_correct_branch "${session_id}"

  commit_view_children "${session_id}" "${commit_id}"

  local forgejo_commit
  forgejo_commit="$(git_commit_find_cherry_pick "${commit_id}")"

  cat <<EOF
# URLs:
#  - Gitea PR: $(commit_url_gitea_pr_or_commit "${commit_id}")
EOF

  if [ -n "${forgejo_commit}" ]; then
    echo "#  - Forgejo:  $(commit_url_forgejo_commit "${forgejo_commit}")"
  fi
}

commit_render_mark() {
  local mark
  mark="${1}"

  case "${mark}" in
    todo)
      echo question
      ;;
    pick)
      echo cherries
      ;;
    skip)
      echo fast_forward
      ;;
    port)
      echo bulb
      ;;
    in-progress)
      echo writing_hand
      ;;
  esac
}

commit_render() {
  local session_id commit_id
  session_id="${1}"
  commit_id="${2}"

  assert_correct_branch "${session_id}"

  local n mark comments forgejo_commit
  n="$(db_commit_get_n "${session_id}" "${commit_id}")"
  mark="$(db_commit_mark_get "${session_id}" "${commit_id}")"
  pr="$(commit_pr_find "${commit_id}")"
  message="$(commit_message_get "${commit_id}")"

  if [ "${mark}" = "pick" ]; then
    forgejo_commit="$(git_commit_find_cherry_pick "${commit_id}")"
  else
    forgejo_commit=""
  fi

  mark="$(commit_render_mark "${mark}")"

  echo "- :${mark}: [\`gitea\`]($(commit_url_gitea_commit "${commit_id}"))${forgejo_commit:+ -> [\`forgejo\`]($(commit_url_forgejo_commit "${forgejo_commit}"))} ${message}${pr:+ ([gitea#${pr}]($(commit_url_gitea_pr "${pr}")))}"

  local comments
  comments="$(db_commit_comments_get "${session_id}" "${commit_id}")"
  if [ -n "${comments}" ]; then
    # shellcheck disable=SC2001
    echo "${comments}" | sed -e "s,^,  ,"
  fi

  local children
  children="$(db_commit_link_list_children "${session_id}" "${commit_id}")"

  for child_id in ${children}; do
    commit_render "${session_id}" "${child_id}" | sed -e "s,^,  ,"
  done
}

commit_render_for_release_notes() {
  local session_id commit_id
  session_id="${1}"
  commit_id="${2}"

  assert_correct_branch "${session_id}"

  local mark pr message forgejo_commit
  mark="$(db_commit_mark_get "${session_id}" "${commit_id}")"
  pr="$(commit_pr_find "${commit_id}")"
  message="$(commit_message_get "${commit_id}")"

  if [ "${mark}" != "pick" ]; then
    echo "Internal error: trying to render release notes for non-picked commit: \`${commit_id}\`" >&2
    exit 1
  fi

  forgejo_commit="$(git_commit_find_cherry_pick "${commit_id}")"

  echo "TODO: [commit]($(commit_url_forgejo_commit "${forgejo_commit}")) ${message}"
}
