# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

commit_resolve_n() {
  local session_id n
  session_id="${1}"
  n="${2}"

  case "${n}" in
    @*)
      db_commit_get_id_for_nth "${session_id}" "${n/@/}"
      return
      ;;
  esac

  current_n="$(db_commit_get_n_for_first_pending "${session_id}")"
  if [ -z "${current_n}" ]; then
    # If no pending commits left, assume we're at the last one.
    current_n="$(db_session_commits_count "${session_id}")"
  fi
  # shellcheck disable=SC2004
  moved_n=$((${current_n} + ${n}))

  if [ ${moved_n} -lt 0 ]; then
    echo "Can't move back ${n/-/} steps, currently at ${current_n}!" >&2
    exit 1
  fi

  local max_commits
  max_commits="$(db_session_commits_count "${session_id}")"
  if [ ${moved_n} -ge "${max_commits}" ]; then
    # shellcheck disable=SC2004
    echo "Can't move ${n/+/} steps forward, only $((${max_commits} - 1)) available, currently at ${current_n}." >&2
    exit 1
  fi

  db_commit_get_id_for_nth "${session_id}" "${moved_n}"
}

commit_resolve_id() {
  local session_id commit_prefix
  session_id="${1}"
  commit_prefix="${2}"

  db_commit_get_full_id "${session_id}" "${commit_prefix}"
}
