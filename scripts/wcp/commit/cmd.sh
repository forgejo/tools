# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

. "${_libdir}/commit/cmd/open.sh"
. "${_libdir}/commit/cmd/info.sh"
. "${_libdir}/commit/cmd/render.sh"
. "${_libdir}/commit/cmd/edit.sh"
. "${_libdir}/commit/cmd/pick.sh"
. "${_libdir}/commit/cmd/unpick.sh"
. "${_libdir}/commit/cmd/skip.sh"
. "${_libdir}/commit/cmd/mark.sh"
. "${_libdir}/commit/cmd/link-unlink.sh"
. "${_libdir}/commit/cmd/in-progress.sh"

cmd_wcp_commit_help() {
  cat <<EOF
wcp [commit] [<COMMIT-ID>] [+N|-N] <SUBCOMMAND> ...

Aliases:
  wcp c
  wcp

Always operates on the latest unlocked session!

Commit selection:
  - <ID>: Selects the commit with the given commit id.
  - @N  : Selects the Nth commit.
  - +N  : Selects the commit N steps ahead of the current one.
  - -N  : Selects the commit N steps behind of the current one.

Subcommands:
  open
  o
    Opens the selected commit in the browser.

  info
  i
    View the summary of the selected commit in the terminal.
  render
  r
    Render the commit as markdown.

  edit
  e
    Edit the commentary of the selected commit.

  link <OTHER-COMMIT> | unlink <OTHER-COMMIT>
  l    ...
    Link / unlink

  pick [--edit] | skip [--reason=<REASON|REF>] | port | in-progress [--pr=<N>] | todo
    Mark the selected commit, and edit its commentary after.

    If picking a commit, and the automatic cherry-pick fails, resolve it manually, and run pick again.

    Options for pick:
      --edit: Stop and edit the comments for the commit after picking it.
              Picking happens without a comment by default.

    Options for skip:
      --reason=<REASON|REF>:
              Skip the commit for <REASON> (currently supported: gitea), or signal
              that it is skipped because it's already in Forgejo. In that case,
              the reason should be a reference in the format: forgejo/forgejo#1.

              The reason is automatically added to the commit comments before
              editing.

    Options for in-progress:
      --pr=<N>:
              The pull request porting the commit to Forgejo.

              A reference is automatically added to the commit comments before
              editing.

  unpick
    Unpick the selected commit.

    This rebases the branch on top of the base branch, and drops the selected commit.
EOF
}

cmd_wcp_commit() {
  local session_id
  session_id="$(db_session_get_id)"

  local commit orig_commit
  orig_commit=""

  if [ -z "${session_id}" ] && { [ "${1:-}" != "help" ] && [ "${1:-}" != "h" ]; }; then
    echo "Error: Unable to continue without a session id." >&2
    exit 1
  fi

  case "${1:-}" in
    +* | -* | @*)
      commit="$(commit_resolve_n "${session_id}" "${1}")"
      orig_commit="${1}"
      shift
      ;;
    help | h)
      cmd_wcp_commit_help
      return
      ;;
    open | o | info | i | edit | e | pick | skip | port | in-progress | todo | unpick | link | l | unlink | render | r)
      commit="$(db_commit_get_id_for_first_pending "${session_id}")"
      orig_commit="${commit}"
      ;;
    *)
      orig_commit="${1}"
      commit="$(commit_resolve_id "${session_id}" "${orig_commit}")"
      shift
      ;;
  esac

  if [ -z "${commit:-}" ]; then
    if [ -z "${orig_commit}" ]; then
      echo "Unable to determine where we are. Did we reach the end of the pending list?!" >&2
    else
      echo "Unable to find commit: '${orig_commit}'" >&2
    fi
    exit 1
  fi

  local subcmd
  subcmd="${1:-}"
  shift || true

  case "${subcmd}" in
    open | o)
      cmd_wcp_commit_open "${commit}"
      ;;
    info | i)
      cmd_wcp_commit_info "${session_id}" "${commit}"
      ;;
    render | r)
      cmd_wcp_commit_render "${session_id}" "${commit}"
      ;;
    edit | e)
      cmd_wcp_commit_edit "${session_id}" "${commit}"
      ;;
    link | l)
      cmd_wcp_commit_link "${session_id}" "${commit}" "$@"
      ;;
    unlink)
      cmd_wcp_commit_unlink "${session_id}" "${commit}" "$@"
      ;;
    pick)
      cmd_wcp_commit_pick "${session_id}" "${commit}" "$@"
      ;;
    unpick)
      cmd_wcp_commit_unpick "${session_id}" "${commit}" "$@"
      ;;
    skip)
      cmd_wcp_commit_skip "${session_id}" "${commit}" "$@"
      ;;
    in-progress)
      cmd_wcp_commit_in_progress "${session_id}" "${commit}" "$@"
      ;;
    port | todo)
      cmd_wcp_commit_mark "${session_id}" "${commit}" "${subcmd}"
      ;;
    *)
      echo "Unknown subcommand: '${subcmd}'" >&2
      cmd_wcp_commit_help >&2
      exit 1
      ;;
  esac
}
