# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "session rebase fails without a sesson" {
  run -1 ${WCP} session rebase
  assert_output "Error: Cannot rebase a session without a session id."
}

@test "session rebase defaults to target-base-branch" {
  run ${WCP} session start --source=gitea

  # Move the forgejo branch further
  git checkout -q forgejo
  git commit --allow-empty -m "moved"
  git checkout -

  assert_not_equal "$(git rev-parse HEAD)" "$(git rev-parse forgejo)"
  run -0 ${WCP} session rebase
  assert_equal "$(git rev-parse HEAD)" "$(git rev-parse forgejo)"

  assert_query 'SELECT "target-base-ref" FROM session WHERE "id"=1' "$(git rev-parse forgejo)"
}

@test "session rebase works with an explicit ref" {
  run ${WCP} session start --source=gitea

  # Move the forgejo branch further, twice
  git checkout -q forgejo
  git commit --allow-empty -m "moved #1"
  git commit --allow-empty -m "moved #2"
  git checkout -

  run -0 ${WCP} session rebase --onto="$(git rev-parse forgejo~1)"
  assert_equal "$(git rev-parse HEAD)" "$(git rev-parse forgejo~1)"

  assert_query 'SELECT "target-base-ref" FROM session WHERE "id"=1' "$(git rev-parse forgejo~1)"
}

@test "session rebase with conflict resolving works" {
  run ${WCP} session start --source=gitea

  # add a commit to the forgejo branch
  git checkout -q forgejo
  echo "new" >conflict-file.txt
  git add conflict-file.txt
  git commit -m "new file: conflict-file"

  # add a confilicting commit to the current branh
  git checkout -
  echo "old" >conflict-file.txt
  git add conflict-file.txt
  git commit -m "new file: conflict-file"

  # Try to rebase..
  run -1 ${WCP} session rebase --onto="$(git rev-parse forgejo)"
  assert_output --partial "CONFLICT"

  # ...skip the conflicting patch
  git rebase --skip

  # Finish the rebase
  run -0 ${WCP} session rebase --onto="$(git rev-parse forgejo)"
  assert_query 'SELECT "target-base-ref" FROM session WHERE "id"=1' "$(git rev-parse forgejo)"
}

@test "session rebase on the wrong branch fails" {
  run ${WCP} session start --source=gitea

  git checkout -q -B wrong-branch-name

  run -1 ${WCP} session rebase --onto="$(git rev-parse forgejo~1)"
  assert_output --regexp "Error: Current branch \(\`.*\`\) is not \`.*\`\."
}
