# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

assert_commit_states() {
  run -0 ${WCP} session info

  assert_output --partial "- Commits: ${1}"
  assert_output --partial "- todo: ${2}"
  assert_output --partial "- pick: ${3}"
  assert_output --partial "- skip: ${4}"
  assert_output --partial "- port: ${5}"
  assert_output --partial "- in-progress: ${6}"
}

@test "session info without a session fails" {
  run -1 ${WCP} session info
}

@test "session info works" {
  run ${WCP} session start --source=gitea --week=2024-42
  run -0 ${WCP} session info
  assert_output "Session #1, for week 2024-42:
  - Targets 'forgejo' (starting at '${_WCP_INITIAL_COMMIT}')
  - Picking from 'gitea' ($(git rev-parse gitea~3)...$(git rev-parse gitea))
  - In progress
  - Commits: 3
    - todo: 3
    - pick: 0
    - skip: 0
    - port: 0
    - in-progress: 0"
}

@test "session info == s i" {
  run ${WCP} session start --source=gitea --week=2024-42
  run -0 ${WCP} s i
  assert_output "$(${WCP} session info)"
}

@test "session info reflects commit states" {
  run ${WCP} session start --source=gitea --week=2024-42

  run ${WCP} commit +0 pick
  assert_commit_states 3 2 1 0 0 0

  run ${WCP} commit -1 unpick
  assert_commit_states 3 3 0 0 0 0

  EDITOR=cat run ${WCP} commit +0 skip
  assert_commit_states 3 2 0 1 0 0

  EDITOR=cat run ${WCP} commit -1 port
  assert_commit_states 3 2 0 0 1 0

  EDITOR=cat run ${WCP} commit -1 in-progress
  assert_commit_states 3 2 0 0 0 1

  EDITOR=cat run ${WCP} commit -1 todo
  assert_commit_states 3 3 0 0 0 0
}

@test "session info reflects locking" {
  run ${WCP} session start --source=gitea
  run ${WCP} session lock

  run -0 ${WCP} session info
  assert_output --partial "- Locked"
}
