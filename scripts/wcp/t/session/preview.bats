# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "session preview without a session fails" {
  run -1 ${WCP} session preview
}

@test "session preview works" {
  run ${WCP} session start --source=gitea --week=2024-42
  run -0 ${WCP} session preview
  assert_output --partial "## Checklist"
  assert_output --partial "## Legend"
  assert_output --partial "## Commits"
  refute_output --partial "## TODO"
}

@test "session preview == s v" {
  run ${WCP} session start --source=gitea --week=2024-42
  run -0 ${WCP} s v
  assert_output "$(${WCP} session preview)"
}

@test "session preview reflects the commit state" {
  run ${WCP} session start --source=gitea --week=2024-42

  run ${WCP} commit pick
  EDITOR=cat run ${WCP} commit skip
  EDITOR=cat run ${WCP} commit skip

  run ${WCP} session preview

  refute_output --regexp "- :question: \[\`gitea\`\]"
  assert_output --regexp "- :fast_forward: \[\`gitea\`\]"
  assert_output --regexp "- :cherries: \[\`gitea\`\]\([^)]*\) -> \[\`forgejo\`\]"
  assert_output --partial "## Skipped"
  refute_output --partial "## TODO"
}

@test "session preview automatically adds a TODO section" {
  run ${WCP} session start --source=gitea --week=2024-42

  EDITOR=cat run ${WCP} commit port

  run ${WCP} session preview

  assert_output --partial "## TODO"
  assert_output --regexp "- :bulb: \[\`gitea\`\]"
}

@test "session preview has a stats summary" {
  run ${WCP} session start --source=gitea --week=2024-42

  EDITOR=cat run ${WCP} commit pick
  EDITOR=cat run ${WCP} commit skip

  run ${WCP} session preview

  assert_output --partial "<summary><h2>Stats</h2></summary>"
  assert_output --partial "We picked **1**, skipped **1**,"
}

@test "session preview's stats has an \"already in forgejo\" flavour" {
  run ${WCP} session start --source=gitea --week=2024-42

  EDITOR=cat run ${WCP} commit skip --reason='forgejo/forgejo#1'
  EDITOR=cat run ${WCP} commit skip

  run ${WCP} session preview

  assert_output --partial "skipped **2** (of which **1** was already in Forgejo!)"
}

@test "Session preview's stats \"already in forgejo\" is case insensitive" {
  run ${WCP} session start --source=gitea --week=2024-42

  export NEW_CONTENTS="- aLrEAdy iN forgejO via c0ffeebabe"
  EDITOR=test-editor run ${WCP} commit skip
  EDITOR=cat run ${WCP} commit skip

  run ${WCP} session preview

  assert_output --partial "skipped **2** (of which **1** was already in Forgejo!)"
}

@test "Session preview's stats \"already in forgejo\" is recognized mid-sentence" {
  run ${WCP} session start --source=gitea --week=2024-42

  export NEW_CONTENTS="already in forgejo: forgejo/forgejo#1"
  EDITOR=test-editor run ${WCP} commit skip
  EDITOR=cat run ${WCP} commit skip

  run ${WCP} session preview

  assert_output --partial "skipped **2** (of which **1** was already in Forgejo!)"
}

@test "session preview's flavour handles plurals" {
  run ${WCP} session start --source=gitea --week=2024-42

  EDITOR=cat run ${WCP} commit skip --reason='forgejo/forgejo#1'
  EDITOR=cat run ${WCP} commit skip
  EDITOR=cat run ${WCP} commit skip --reason='forgejo/forgejo#2'

  run ${WCP} session preview

  assert_output --partial "skipped **3** (of which **2** were already in Forgejo!)"
}

@test "session preview --dashboard" {
  run ${WCP} session start --source=gitea --week=2024-42

  EDITOR=cat run "${WCP}" commit in-progress
  EDITOR=cat run "${WCP}" commit port

  run -0 ${WCP} session preview --dashboard
  refute_output --regexp ":\(question\|fast_forward\|cherries\|writing_hand\):"
  assert_output --partial ":bulb"
  assert_output --partial "[Week 2024-42]("
}

@test "session preview on the wrong branch fails" {
  run ${WCP} session start --source=gitea

  git checkout -q -B wrong-branch-name

  run -1 ${WCP} session preview
  assert_output --regexp "Error: Current branch \(\`.*\`\) is not \`.*\`\."
}
