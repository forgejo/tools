# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "trying to edit release-notes for a new session fails" {
  run ${WCP} session start --source=gitea
  run -1 ${WCP} session release-notes

  assert_output "ERROR: There is no pull request for this session yet. Create one before writing release notes."
}

@test "session release-notes == s rn" {
  run ${WCP} session start --source=gitea
  run -1 ${WCP} s rn

  assert_output "ERROR: There is no pull request for this session yet. Create one before writing release notes."
}

@test "session release-notes fail if there are TODO items" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42
  run -0 ${WCP} pick
  run -0 ${WCP} session checklist set self-pr 42

  EDITOR=cat run -1 ${WCP} session release-notes

  assert_output --partial "Please finalize the release notes and run this command again."
}

@test "session release-notes work" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42
  run -0 ${WCP} pick
  run -0 ${WCP} session checklist set self-pr 42

  export NEW_CONTENTS="fix: [commit](some-link) description"
  EDITOR=test-editor run ${WCP} session release-notes

  assert_output --partial "chore(release-notes): notes for the week 2024-42 weekly cherry pick"
}
