# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "session list-commits without a session fails" {
  run -1 ${WCP} session list-commits
  assert_output "Error: Cannot list commits without a session id."
}

@test "session list-commits" {
  run ${WCP} session start --source=gitea --week=2024-42
  run -0 ${WCP} session list-commits

  assert_output "$(git rev-parse gitea~2) :todo: modified another-file.c
$(git rev-parse gitea~1) :todo: modified gitea-specific files
$(git rev-parse gitea) :todo: an empty commit"
}

@test "session list-commits == s ls" {
  run ${WCP} session start --source=gitea --week=2024-42
  run -0 ${WCP} s ls
  assert_output "$(${WCP} session list-commits)"
}

@test "session pending == s p" {
  run ${WCP} session start --source=gitea --week=2024-42
  run -0 ${WCP} s p
  assert_output "$(${WCP} session pending)"
}

@test "session list-commits lists all commits" {
  run ${WCP} session start --source=gitea --week=2024-42

  EDITOR=cat run ${WCP} commit skip

  run -0 ${WCP} session list-commits

  assert_output "$(git rev-parse gitea~2) :skip: modified another-file.c
$(git rev-parse gitea~1) :todo: modified gitea-specific files
$(git rev-parse gitea) :todo: an empty commit"
}

@test "session list-commits --mark= works" {
  run ${WCP} session start --source=gitea --week=2024-42

  local commit_id
  commit_id="$(git rev-parse gitea~2)"

  do_mark() {
    EDITOR=cat run ${WCP} commit "${commit_id}" "${1}"
    run ${WCP} session list-commits --mark="${1}"
    assert_output --regexp "^${commit_id} :${1}: .*\$"
    EDITOR=cat run ${WCP} commit "${commit_id}" "todo"
  }

  do_mark skip
  do_mark port
  do_mark in-progress
  do_mark pick
}

@test "session pending == session list-commits --mark=todo" {
  run ${WCP} session start --source=gitea --week=2024-42
  EDITOR=cat run ${WCP} commit +1 skip

  run ${WCP} session pending
  assert_output "$(${WCP} session list-commits --mark=todo)"
}
