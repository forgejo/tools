# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "session starting with defaults" {
  run ${WCP} session start --source=gitea --week=2024-42
  assert_output "Switched to a new branch 'wcp/2024-42'
1"

  assert_query 'SELECT "target-base-branch" FROM session' "forgejo"
  assert_query 'SELECT "target-base-ref" FROM session' "${_WCP_INITIAL_COMMIT}"
  assert_query 'SELECT "source-branch" FROM session' "gitea"
  assert_query 'SELECT "source-start-ref" FROM session' "${_WCP_INITIAL_COMMIT}"
  assert_query 'SELECT "source-end-ref" FROM session' "$(git rev-parse --revs-only gitea)"
  assert_query 'SELECT "locked" FROM session' '0'
}

@test "session starting with custom id" {
  run ${WCP} session --id=42 start --source=gitea --week=2024-42
  assert_output "Switched to a new branch 'wcp/2024-42'
42"
}

@test "session starting with custom params" {
  run ${WCP} session start \
    --week=2024-42 \
    --source=gitea \
    --note="$(printf "multi-line\nnote\n")" \
    --from="$(git rev-parse --revs-only gitea~1)"

  assert_query 'SELECT "week" FROM session' "2024-42"
  assert_query 'SELECT "note" FROM session' "multi-line
note"
  assert_query 'SELECT "source-start-ref" FROM session' "$(git rev-parse --revs-only gitea~1)"
  assert_query 'SELECT "source-end-ref" FROM session' "$(git rev-parse --revs-only gitea)"
}

@test "session starting fails if source branch doesn't exist" {
  run -1 ${WCP} session start --source=does-not-exist
  assert_output --partial "source branch does not exist"
}

@test "session starting fails if the target branch doesn't exist" {
  run -1 ${WCP} session start --target=does-not-exist --source=gitea
  assert_output --partial "target branch does not exist"
}

@test "session starting re-uses the previous end ref" {
  run ${WCP} session start --source=gitea --week=2024-42-1
  run ${WCP} session lock

  run ${WCP} session start --source=gitea --week=2024-42-2
  assert_output "Switched to a new branch 'wcp/2024-42-2'
2"

  assert_query 'SELECT "source-start-ref" FROM session WHERE id=2' "$(git rev-parse --revs-only gitea)"
  assert_query 'SELECT "source-end-ref" FROM session WHERE id=2' "$(git rev-parse --revs-only gitea)"
}

@test "session start populates the previous PR" {
  run ${WCP} session start --source=gitea --week=2024-42-1
  run ${WCP} session checklist set self-pr 4242
  run ${WCP} session lock

  run ${WCP} session start --source=gitea --week=2024-42-2
  assert_output "Switched to a new branch 'wcp/2024-42-2'
2"

  assert_query 'SELECT "previous-pick-pr" FROM checklist WHERE "session-id" = 2' 4242
}

@test "sessions can be deleted" {
  run ${WCP} session start --source=gitea --week=2024-42-1
  run ${WCP} session start --source=gitea --week=2024-42-2

  run ${WCP} session --id=2 delete
  assert_query 'SELECT id FROM session' '1'

  run ${WCP} session delete
  assert_query 'SELECT COUNT(id) FROM session' '0'
}
