# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

_WCP_GIT_ROOT="${_BATS_TEST_WORKDIR}/repo"
mkdir -p "${_WCP_GIT_ROOT}"
cd "${_WCP_GIT_ROOT}"
git init -q -b forgejo
git config user.name "Jane Doe"
git config user.email "jane.doe@example.com"

# Create the base branch
mkdir -p .gitea .github docs

echo "1" >docs/1.md
echo "2" >.gitea/some-file.txt
echo "3" >.github/some-file.txt
echo "4" >some-file.txt
echo "5" >another-file.c

git add .
git commit -q -m "Initial import"

_WCP_INITIAL_COMMIT="$(git rev-parse --revs-only HEAD)"

# Create a branch we'll cherry pick from
git switch -q -C gitea forgejo

echo "modified" >another-file.c
git add another-file.c
git commit -q -m "modified another-file.c"

echo "modified" >docs/1.md
echo "modified" >.gitea/some-file.txt
echo "modified" >.github/some-file.txt

git add docs .gitea .github
git commit -q -m "modified gitea-specific files"

git commit --allow-empty -q -m "an empty commit"

# Switch back to the main branch
git checkout -q forgejo
