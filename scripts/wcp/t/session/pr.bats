# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "session pr sync without a session fails" {
  run -1 ${WCP} session pr sync -n
}

@test "session pr sync without configuration fails" {
  run ${WCP} session start --source=gitea
  run -1 ${WCP} session pr sync -n
}

@test "session pr sync (dry-run), with only the token set" {
  run ${WCP} session start --source=gitea

  export WCP_FORGE_API_TOKEN="not-a-valid-token"

  run -0 ${WCP} session pr sync -n
  assert_output --partial "git push \"origin\""
  assert_output --partial '"https://codeberg.org/api/v1/repos/forgejo/forgejo/pulls/${WCP_PULL_REQUEST_ID}"'
  refute_output --partial "# Add labels to the pull request"
  refute_output --partial "# Checklist complete, remove the WIP prefix"
}

@test "session pr sync (dry-run), with all config values set" {
  run ${WCP} session start --source=gitea

  export WCP_PULL_REQUEST_REMOTE="test-remote"
  export WCP_FORGE="https://forge.example.com"
  export WCP_BASE_REPO="org/test"
  export WCP_FORGE_API_TOKEN="not-a-valid-token"
  export WCP_PULL_REQUEST_LABEL_IDS="69"

  run -0 ${WCP} session pr sync -n
  assert_output --partial "git push \"${WCP_PULL_REQUEST_REMOTE}\""
  assert_output --partial "\"${WCP_FORGE}/api/v1/repos/${WCP_BASE_REPO}/pulls/\${WCP_PULL_REQUEST_ID}\""
  assert_output --partial "\"${WCP_FORGE}/api/v1/repos/${WCP_BASE_REPO}/issues/\${WCP_PULL_REQUEST_ID}/labels\""
  assert_output --partial "-o title=\"[gitea] week"
  refute_output --partial "# Checklist complete, remove the WIP prefix"
}

@test "session pr sync (dry-run) with an (almost) complete checklist" {
  run ${WCP} session start --source=gitea

  export WCP_FORGE_API_TOKEN="not-a-valid-token"

  set_item() {
    run ${WCP} session checklist set "${1}" "${2}"
  }

  set_item interest-check 1
  set_item previous-pick-pr 4242
  set_item self-pr 8484
  set_item ci-passed 1
  set_item e2e-label-pr 1234
  set_item e2e-result-action-run 16384

  run -0 ${WCP} session pr sync -n
  assert_output --partial "-o title=\"[gitea] week"
  refute_output --partial "# Checklist complete, remove the WIP prefix"
}

@test "session pr sync (dry-run) syncs the correct branch" {
  run ${WCP} session start --source=gitea --week=2024-42

  export WCP_FORGE_API_TOKEN="not-a-valid-token"

  run -0 ${WCP} session pr sync -n
  assert_output --partial "git push \"origin\" \"wcp/2024-42:refs/for/forgejo/wcp/2024-42\""
}

@test "session pr sync on the wrong branch fails" {
  run ${WCP} session start --source=gitea

  git checkout -q -B wrong-branch-name

  run -1 ${WCP} session pr sync -n
  assert_output --regexp "Error: Current branch \(\`.*\`\) is not \`.*\`\."
}
