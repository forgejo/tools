# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "locking without a session fails" {
  run -1 ${WCP} session lock
  assert_output "Error: Cannot lock/unlock a session without a session id."
}

@test "locking an existing session works" {
  run ${WCP} session start --source=gitea
  run -0 ${WCP} session lock
  assert_query 'SELECT locked FROM session WHERE id=1' '1'
}

@test "unlocking without a session fails" {
  run -1 ${WCP} session unlock
  assert_output "Error: Cannot lock/unlock a session without a session id."
}

@test "unlocking an existing session works" {
  run ${WCP} session start --source=gitea
  run -0 ${WCP} session lock
  run -0 ${WCP} session unlock

  assert_query 'SELECT locked FROM session WHERE id=1' '0'
}

@test "locking non-existent session with targeting fails" {
  run -0 ${WCP} session start --source=gitea
  run -1 ${WCP} session --id=42 lock
}
