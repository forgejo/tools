# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "session ci without a session fails" {
  run -1 ${WCP} session ci status
}

@test "session ci without a pull request fails" {
  run ${WCP} session start --source=gitea

  run -1 ${WCP} session ci status
  assert_output "Error: No pull request set for the session yet!"
}
