# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "session add-commits fails without a sesson" {
  run -1 ${WCP} session add-commits
  assert_output "Error: Cannot add commits to a session without a session id."
}

@test "session add-commits pulls from the source branch by default" {
  run ${WCP} session start --source=gitea

  local initial_commit_count
  initial_commit_count="$(${WCP} session list-commits | wc -l)"

  # Add new commits to the source
  git checkout -q gitea
  git commit --allow-empty -m "new commit"
  git checkout -

  run -0 ${WCP} session add-commits

  local commit_count
  commit_count="$(${WCP} session list-commits | wc -l)"

  assert [ "${commit_count}" -gt "${initial_commit_count}" ]
}

@test "session add-commits --until" {
  run ${WCP} session start --source=gitea

  local initial_commit_count
  initial_commit_count="$(${WCP} session list-commits | wc -l)"

  # Add new commits to the source
  git checkout -q gitea
  git commit --allow-empty -m "new commit"
  git commit --allow-empty -m "new commit"
  git checkout -

  run -0 ${WCP} session add-commits --until="$(git rev-parse gitea~1)"

  local commit_count
  commit_count="$(${WCP} session list-commits | wc -l)"

  assert [ $((${initial_commit_count} + 1)) -eq "${commit_count}" ]
}

@test "session add-commits on the wrong branch fails" {
  run ${WCP} session start --source=gitea

  git checkout -q -B wrong-branch-name

  run -1 ${WCP} session add-commits --until="$(git rev-parse gitea~1)"
  assert_output --regexp "Error: Current branch \(\`.*\`\) is not \`.*\`\."
}
