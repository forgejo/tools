# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "session add-commit fails without a sesson" {
  run -1 ${WCP} session add-commit
  assert_output "Error: Cannot add commits to a session without a session id."
}

@test "session add-commit fails without a REF" {
  run ${WCP} session start --source=gitea

  run -1 ${WCP} session add-commit
  assert_output "Error: Need a commit reference to add."
}

@test "session add-commit fails with an invalid REF" {
  run ${WCP} session start --source=gitea

  run -1 ${WCP} session add-commit "invalid-ref"
  assert_output "Error: Unable to resolve reference: 'invalid-ref'."
}

@test "session add-commit works" {
  run ${WCP} session start --source=gitea

  local initial_end
  initial_end="$(git rev-parse -q --verify gitea)"
  local initial_commit_count
  initial_commit_count="$(${WCP} session list-commits | wc -l)"

  # Add new commits to the source
  git checkout -q gitea
  git commit --allow-empty -m "new commit"
  git commit --allow-empty -m "new commit"
  git checkout -

  run -0 ${WCP} session add-commit gitea~1

  local commit_count
  commit_count="$(${WCP} session list-commits | wc -l)"

  local current_end
  current_end="$(${WCP} session info | grep "Picking from" | sed -e "s,^.*\.\.\.\(.*\)),\1,")"

  # We have more commits in the session
  assert [ $((${initial_commit_count} + 1)) -eq "${commit_count}" ]
  # ...but the end is unmodified.
  assert [ "${initial_end}" = "${current_end}" ]
}

@test "session add-commit on the wrong branch fails" {
  run ${WCP} session start --source=gitea

  git checkout -q -B wrong-branch-name

  run -1 ${WCP} session add-commit gitea~1
  assert_output --regexp "Error: Current branch \(\`.*\`\) is not \`.*\`\."
}
