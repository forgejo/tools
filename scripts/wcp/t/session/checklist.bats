# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "new sessions have an empty checklist" {
  run ${WCP} session start --source=gitea

  run ${WCP} session checklist view

  assert_output --partial '- [ ]'
  refute_output --partial '- [x]'
}

@test "session checklist view == s cl v" {
  run ${WCP} session start --source=gitea

  run ${WCP} s cl v
  assert_output "$(${WCP} session checklist view)"
}

@test "session checklist == session checklist view" {
  run ${WCP} session start --source=gitea

  run ${WCP} session checklist
  assert_output "$(${WCP} session checklist view)"
}

@test "session checklist set = s cl s" {
  run ${WCP} session start --source=gitea

  run ${WCP} s cl s ci-passed true
  run ${WCP} session checklist view
  assert_output --partial "- [x] have the PR pass the CI"
}

@test "session checklist's interest check is no longer rendered" {
  run ${WCP} session start --source=gitea
  run ${WCP} session checklist view

  refute_output --partial "particular interest to someone"
}

@test "filling in the checklist works" {
  run ${WCP} session start --source=gitea

  set_item() {
    run ${WCP} session checklist set "${1}" "${2}"
  }

  set_item interest-check 1
  set_item previous-pick-pr 4242
  set_item self-pr 8484
  set_item ci-passed 1
  set_item e2e-label-pr 1234
  set_item e2e-result-action-run 16384
  set_item reviewers-assigned 1
  set_item matrix-48h-call-url https://example.com
  set_item release-notes true

  run ${WCP} session checklist view
  assert_output --partial "- [x]"
  refute_output --partial "- [ ]"
}
