# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "common/setup.sh"
}

teardown() {
  load "common/teardown.sh"
}

@test "the init file is in use" {
  ! test -e "${_WCP_DBFILE}"
  ${WCP} help >/dev/null
  test -e "${_WCP_DBFILE}"
}
