# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "common/setup.sh"
}

teardown() {
  load "common/teardown.sh"
}

@test "session = s" {
  run ${WCP} s help
  assert_output "$(${WCP} session help)"
}

@test "commit = c" {
  run ${WCP} c help
  assert_output "$(${WCP} commit help)"
}

@test "notes = n" {
  run ${WCP} n help
  assert_output "$(${WCP} notes help)"
}
