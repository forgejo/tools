# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

assert_query() {
  local query expected actual
  query="${1}"
  expected="${2}"

  actual="$(sqlite3 "${_WCP_DBFILE}" "${query}")"

  assert_equal "${actual}" "${expected}"
}
