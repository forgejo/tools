# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

local workdir
workdir="$(mktemp -d)"
cat >"${workdir}/wcp.init.sh" <<EOF
CONFIG_DIR="${workdir}/config"
CONFIG_FILE="\${CONFIG_DIR}/config.sh"
STATE_DIR="${workdir}/state"
DBFILE="\${STATE_DIR}/db.sqlite"
EOF

_BATS_TEST_WORKDIR="${workdir}"
export WCP_INIT_FILE="${workdir}/wcp.init.sh"
WCP="${BATS_CWD}/scripts/weekly-cherry-pick.sh"

_WCP_STATE_DIR="${workdir}/state"
_WCP_DBFILE="${_WCP_STATE_DIR}/db.sqlite"

bats_load_library bats-support
bats_load_library bats-assert

bats_require_minimum_version 1.5.0

mkdir -p "${_BATS_TEST_WORKDIR}/bin"
PATH="${_BATS_TEST_WORKDIR}/bin:${PATH}"

# Set up a dummy editor
cat >"${_BATS_TEST_WORKDIR}/bin/test-editor" <<EOF
#! /bin/sh
echo "\${NEW_CONTENTS}" >\${2:-\${1}}
EOF
chmod +x "${_BATS_TEST_WORKDIR}/bin/test-editor"
