# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "commit render fails without a session" {
  run -1 ${WCP} commit render
  assert_output "Error: Unable to continue without a session id."
}

@test "commit render works" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -0 ${WCP} commit render

  assert_output --regexp ":question: \[\`gitea\`\](.*) modified another-file.c"
}

@test "commit render == c r" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -0 ${WCP} c r
  assert_output "$(${WCP} commit render)"
}

@test "commit render groups linked commits" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run ${WCP} commit @1 link @0
  run ${WCP} commit render

  assert_line --regexp '^- :question: \[`gitea`\](.*) modified another-file.c'
  assert_line --regexp '^  - :question: \[`gitea`\](.*) modified gitea-specific files'
}

@test "commit render on the wrong branch fails" {
  run ${WCP} session start --source=gitea

  git checkout -q -B wrong-branch-name

  run -1 ${WCP} commit render
  assert_output --regexp "Error: Current branch \(\`.*\`\) is not \`.*\`\."
}
