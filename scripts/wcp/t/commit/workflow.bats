# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "workflow: a clean pick" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -0 ${WCP} pick

  run ${WCP} commit -1 info
  assert_output --partial "pick#0"
  assert_output --partial "- :pick:"
  assert_output --partial "  - Gitea PR: https://github.com/go-gitea/gitea/"
  assert_output --partial "  - Forgejo:  https://codeberg.org/forgejo/forgejo/commit/"
}

@test "workflow: pick without --edit does not call the editor" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  export NEW_CONTENTS="hello world"
  EDITOR=test-editor run -0 ${WCP} commit pick

  run -0 ${WCP} commit -1 info
  refute_output --partial "hello world"
}

@test "workflow: pick with an unknown option does not call the editor" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  export NEW_CONTENTS="hello world"
  EDITOR=test-editor run -0 ${WCP} commit pick --some-random-option

  run -0 ${WCP} commit -1 info
  refute_output --partial "hello world"
}

@test "workflow: pick with --edit does call the editor" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  export NEW_CONTENTS="hello world"
  EDITOR=test-editor run -0 ${WCP} commit pick --edit

  run -0 ${WCP} commit -1 info
  assert_output --partial "hello world"
}

@test "workflow: skipping over a gitea-specific commit" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  EDITOR=cat run -0 ${WCP} commit +1 skip

  run ${WCP} commit +1 info
  assert_output --partial "pick#1"
  assert_output --partial "- :skip:"
  assert_output --partial "- gitea specific"
}

@test "workflow: conflict handling" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  EDITOR=cat run -1 ${WCP} commit +3 pick
  assert_output --partial "CONFLICT"

  echo "resolved" >some-file.c
  git add some-file.c
  git cherry-pick --continue --no-edit

  EDITOR=cat run -0 ${WCP} commit +3 pick

  run ${WCP} commit +3 info
  assert_output --partial "pick#3"
  assert_output --partial "- :pick:"
  assert_output --partial "  - Gitea PR: https://github.com/go-gitea/gitea/"
  assert_output --partial "  - Forgejo:  https://codeberg.org/forgejo/forgejo/commit/"
}

@test "workflow: in-progress cherry picks stop other actions" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  EDITOR=cat run -1 ${WCP} commit +3 pick
  assert_output --partial "CONFLICT"

  assert_for_mark() {
    local mark
    mark="${1}"

    EDITOR=cat run -1 ${WCP} commit +3 "${mark}"
    assert_output "Error: A cherry pick is already in progress, finish that first!"
  }

  for mark in todo skip port in-progress; do
    assert_for_mark "${mark}"
  done
}

@test "workflow: unpick" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  local head_commit
  head_commit="$(git rev-parse HEAD)"

  run -0 ${WCP} commit pick
  assert_not_equal "${head_commit}" "$(git rev-parse HEAD)"

  run -0 ${WCP} commit -1 unpick
  assert_equal "${head_commit}" "$(git rev-parse HEAD)"
}

@test "workflow: unpick from the middle" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  # First pick
  run -0 ${WCP} commit pick

  # Second pick
  EDITOR=cat run -1 ${WCP} commit +2 pick

  echo "resolved" >some-file.c
  git add some-file.c
  EDITOR=cat git cherry-pick --continue --no-edit

  EDITOR=cat run -0 ${WCP} commit +2 pick

  run git log --format=oneline forgejo...
  assert_output --partial "modified another-file"
  assert_output --partial "tweaked some-file.c"

  # Unpick the first one
  run -0 ${WCP} commit $(git rev-parse gitea~3) unpick

  run git log --format=oneline forgejo...
  refute_output --partial "modified another-file"
  assert_output --partial "tweaked some-file.c"

  run ${WCP} session list-commits --mark=pick
  assert_output "$(git rev-parse gitea) :pick: tweaked some-file.c (conflict) (#1984)"
}

@test "workflow: commit grouping via linking" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run ${WCP} commit @1 link @0
  run ${WCP} commit @2 link @0

  run ${WCP} session preview
  assert_line --regexp '^- :question: \[`gitea`\](.*) tweaked'
  assert_line --regexp '^- :question: \[`gitea`\](.*) modified another-file.c'
  assert_output --regexp '- :question: \[`gitea`\](.*) modified another-file.c
  - :question: .* modified gitea-specific files
  - :question: .* added a-new-file.txt
'
}
