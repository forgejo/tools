# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "commit open fails without a session" {
  run -1 ${WCP} commit open
  assert_output "Error: Unable to continue without a session id."
}

@test "commit open without a PR" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -0 ${WCP} commit open
  assert_output --regexp "^https://github.com/go-gitea/gitea/commit/"
}

@test "commit open with a PR" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -0 ${WCP} commit $(git rev-parse gitea) open
  assert_output --regexp "^https://github.com/go-gitea/gitea/pull/"
}

@test "commit open == c o" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run ${WCP} c o
  assert_output "$(${WCP} commit open)"
}
