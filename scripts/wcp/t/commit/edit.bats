# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "commit edit fails without a session" {
  run -1 ${WCP} commit edit
  assert_output "Error: Unable to continue without a session id."
}

@test "commit edit works" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  export NEW_CONTENTS="- hello world"
  EDITOR=test-editor run -0 ${WCP} commit edit

  run -0 ${WCP} commit info
  assert_output --partial "  ${NEW_CONTENTS}"
}

@test "commit edit == c e" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  export NEW_CONTENTS="- hello world"
  EDITOR=test-editor run -0 ${WCP} c e

  run -0 ${WCP} commit info
  assert_output --partial "  ${NEW_CONTENTS}"
}

@test "commit edit ignores commented lines" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  export NEW_CONTENTS="- hello world # a trailing comment
# This line is ignored!"
  EDITOR=test-editor run -0 ${WCP} commit edit

  run -0 ${WCP} commit info
  assert_output --partial "# a trailing comment"
  refute_output --partial "This line is ignored!"
}

@test "commit edit on the wrong branch fails" {
  run ${WCP} session start --source=gitea

  git checkout -q -B wrong-branch-name

  export NEW_CONTENTS="nope"
  EDITOR=test-editor run -1 ${WCP} commit edit
  assert_output --regexp "Error: Current branch \(\`.*\`\) is not \`.*\`\."
}
