# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "commit linking fails without a session-id" {
  run -1 ${WCP} commit @1 link @0
  assert_output "Error: Unable to continue without a session id."
}

@test "commit unlinking fails without a session-id" {
  run -1 ${WCP} commit @1 unlink @0
  assert_output "Error: Unable to continue without a session id."
}

@test "commit linking works" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  assert_query "SELECT COUNT(*) FROM links" "0"
  run -0 ${WCP} commit @1 link @0
  assert_query "SELECT COUNT(*) FROM links" "1"
}

@test "commit linking works (alias)" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  assert_query "SELECT COUNT(*) FROM links" "0"
  run -0 ${WCP} commit @1 l @0
  assert_query "SELECT COUNT(*) FROM links" "1"
}

@test "commit unlinking works" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -0 ${WCP} commit @1 link @0
  run -0 ${WCP} commit @1 unlink @0
  assert_query "SELECT COUNT(*) FROM links" "0"
}
