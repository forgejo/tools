# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "commit info fails without a session" {
  run -1 ${WCP} commit info
  assert_output "Error: Unable to continue without a session id."
}

@test "commit info works" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -0 ${WCP} commit info

  assert_output --partial "# pick#0 (gitea@"
  assert_output --partial ":todo: modified another-file.c"
}

@test "commit info == c i" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -0 ${WCP} c i
  assert_output "$(${WCP} commit info)"
}

@test "commit info on the wrong branch fails" {
  run ${WCP} session start --source=gitea

  git checkout -q -B wrong-branch-name

  run -1 ${WCP} commit info
  assert_output --regexp "Error: Current branch \(\`.*\`\) is not \`.*\`\."
}
