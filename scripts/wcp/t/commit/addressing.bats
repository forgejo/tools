# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "commit relative addressing works" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -0 ${WCP} commit +1 info
  assert_output --partial "# pick#1 (gitea@"

  run -0 ${WCP} commit -0 info
  assert_output --partial "# pick#0 (gitea@"
}

@test "commit relative addressing range checks" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -1 ${WCP} commit +42 info
  assert_output "Can't move 42 steps forward, only 3 available, currently at 0."

  run -1 ${WCP} commit -42 info
  assert_output "Can't move back 42 steps, currently at 0!"
}

@test "commit full-id addressing works" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -0 ${WCP} commit $(git rev-parse gitea~1) info
  assert_output --partial "# pick#2 (gitea@"
}

@test "commit partial-id addressing works" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -0 ${WCP} commit $(git rev-parse --short gitea~1) info
  assert_output --partial "# pick#2 (gitea@"
}

@test "commit id addressing properly fails" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -1 ${WCP} commit "this-revision-makes-no-sense" info
  assert_output "Unable to find commit: 'this-revision-makes-no-sense'"
}

@test "commit direct positional addressing works" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -0 ${WCP} commit @1 info
  assert_output --partial "# pick#1 (gitea@"
}

@test "commit direct positional addressing range checks" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  run -1 ${WCP} commit @-1 info
  assert_output "Unable to find commit: '@-1'"

  run -1 ${WCP} commit @100 info
  assert_output "Unable to find commit: '@100'"
}

@test "commit fails informatively at the end of the todo list" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  EDITOR=cat run ${WCP} commit skip
  EDITOR=cat run ${WCP} commit skip
  EDITOR=cat run ${WCP} commit skip
  EDITOR=cat run ${WCP} commit skip

  run -1 ${WCP} commit info
  assert_output "Unable to determine where we are. Did we reach the end of the pending list?!"
}

@test "commit can relatively address backwards at the end" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  EDITOR=cat run ${WCP} commit skip
  EDITOR=cat run ${WCP} commit skip
  EDITOR=cat run ${WCP} commit skip
  EDITOR=cat run ${WCP} commit skip

  run -0 ${WCP} commit -1 info
}
