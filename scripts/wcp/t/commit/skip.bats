# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "commit skip fails without a session" {
  EDITOR=cat run -1 ${WCP} commit skip
  assert_output "Error: Unable to continue without a session id."
}

@test "commit skip works" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  export NEW_CONTENTS="- hello world"
  EDITOR=test-editor run -0 ${WCP} commit skip

  run -0 ${WCP} commit -1 info
  assert_output --partial "  ${NEW_CONTENTS}"
}

@test "commit skip --reason=gitea works" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  EDITOR=cat run -0 ${WCP} commit skip --reason=gitea

  run -0 ${WCP} commit -1 info
  assert_output --partial "- :skip: modified another-file.c"
  assert_output --partial "- gitea specific"
}

@test "commit skip --reason='gitea UI' works" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  EDITOR=cat run -0 ${WCP} commit skip --reason='gitea UI'

  run -0 ${WCP} commit -1 info
  assert_output --partial "- :skip: modified another-file.c"
  assert_output --partial "- gitea UI specific"
}

@test "commit skip --reason=forgejo/forgejo#N works" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  EDITOR=cat run -0 ${WCP} commit skip --reason='forgejo/forgejo#8192'

  run -0 ${WCP} commit -1 info
  assert_output --partial "- :skip: modified another-file.c"
  assert_output --partial "- Already in Forgejo: forgejo/forgejo#8192"
}

@test "commit skip on the wrong branch fails" {
  run ${WCP} session start --source=gitea

  git checkout -q -B wrong-branch-name

  EDITOR=cat run -1 ${WCP} commit skip --reason="wrong branch"
  assert_output --regexp "Error: Current branch \(\`.*\`\) is not \`.*\`\."
}

@test "commit skip does not overwrite existing comments" {
  run ${WCP} session start --source=gitea

  export NEW_CONTENTS="- hello world"
  EDITOR=test-editor run -0 ${WCP} commit port

  EDITOR=cat run -0 ${WCP} commit -1 skip

  run -0 ${WCP} commit -1 info
  assert_output --partial "  ${NEW_CONTENTS}"
}

@test "commit skip --reason= appends to existing comments" {
  run ${WCP} session start --source=gitea

  export NEW_CONTENTS="- hello world"
  EDITOR=test-editor run -0 ${WCP} commit port

  EDITOR=cat run -0 ${WCP} commit -1 skip --reason=forgejo/forgejo#1024

  run -0 ${WCP} commit -1 info
  assert_output --partial "  ${NEW_CONTENTS}"
  assert_output --partial "forgejo/forgejo#1024"
}
