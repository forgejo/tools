# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "../common/setup.sh"
  load "../common/assert-query.sh"
  load "setup.sh"
}

teardown() {
  load "../common/teardown.sh"
}

@test "commit port fails without a session" {
  EDITOR=cat run -1 ${WCP} commit port
  assert_output "Error: Unable to continue without a session id."
}

@test "commit port works" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  export NEW_CONTENTS="- hello world"
  EDITOR=test-editor run -0 ${WCP} commit port

  run -0 ${WCP} commit -1 info
  assert_output --partial "  ${NEW_CONTENTS}"
}

@test "commit port on the wrong branch fails" {
  run ${WCP} session start --source=gitea

  git checkout -q -B wrong-branch-name

  EDITOR=cat run -1 ${WCP} commit port
  assert_output --regexp "Error: Current branch \(\`.*\`\) is not \`.*\`\."
}

@test "db list-portables lists commit ported PRs" {
  run ${WCP} session start --source=gitea --from="${_WCP_INITIAL_COMMIT}" --week=2024-42

  EDITOR=cat run -0 ${WCP} commit port

  run -0 ${WCP} db list-portables
  assert_output --partial "## [Week 2024-42]"
  assert_output --partial "[\`gitea"
}
