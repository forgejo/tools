# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

# Set up a dummy xdg-open
cat >"${_BATS_TEST_WORKDIR}/bin/xdg-open" <<EOF
#! /bin/sh
echo "\$@"
EOF
chmod +x "${_BATS_TEST_WORKDIR}/bin/xdg-open"

# Setup the git repo for the tests

_WCP_GIT_ROOT="${_BATS_TEST_WORKDIR}/repo"
mkdir -p "${_WCP_GIT_ROOT}"
cd "${_WCP_GIT_ROOT}"
git init -q -b forgejo
git config user.name "Jane Doe"
git config user.email "jane.doe@example.com"

# Create the base branch
mkdir -p .gitea .github docs

echo "1" >docs/1.md
echo "2" >.gitea/some-file.txt
echo "3" >.github/some-file.txt
echo "4" >some-file.txt
echo "5" >another-file.c

git add .
git commit -q -m "Initial import"

_WCP_INITIAL_COMMIT="$(git rev-parse --revs-only HEAD)"

# Create a branch we'll cherry pick from
git switch -q -C gitea forgejo

echo "modified" >another-file.c
git add another-file.c
git commit -q -m "modified another-file.c"

echo "modified" >docs/1.md
echo "modified" >.gitea/some-file.txt
echo "modified" >.github/some-file.txt

git add docs .gitea .github
git commit -q -m "modified gitea-specific files"

echo "mooooore" >a-new-file.txt
git add a-new-file.txt
git commit -q -m "added a-new-file.txt"

echo "tweaked" >some-file.c
git add some-file.c
git commit -q -m "tweaked some-file.c (conflict) (#1984)"

# Switch back to the main branch
git checkout -q forgejo

echo "modified" >some-file.c
git add some-file.c
git commit -q -m "modified some-file.c"
