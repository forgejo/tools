# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "common/setup.sh"
  load "session/setup.sh"
}

teardown() {
  load "common/teardown.sh"
}

@test "database export works" {
  ${WCP} db export >"${_BATS_TEST_WORKDIR}/db.sql"

  run cat "${_BATS_TEST_WORKDIR}/db.sql"
  assert_output --partial "CREATE TABLE"
}

@test "database import works" {
  # Create an export with additional data
  ${WCP} session start --source=gitea
  ${WCP} db export >"${_BATS_TEST_WORKDIR}/db.sql"

  # Remove the database
  rm -f "${_WCP_DBFILE}"

  # Verify we have no db
  run -1 ${WCP} session info

  # Import the database back
  run -0 ${WCP} db import <"${_BATS_TEST_WORKDIR}/db.sql"

  run -0 ${WCP} session info
}

@test "database push with explicit username works" {
  run ${WCP} db push -n test-user

  assert_output --partial "https://codeberg.org/api/packages/test-user/generic/wcp-database/current/db.sql"
}

@test "database pull with explicit username works" {
  run ${WCP} db pull -n test-user

  assert_output --partial "https://codeberg.org/api/packages/test-user/generic/wcp-database/current/db.sql"
}

@test "database push/pull username autodetection works" {
  mkdir -p "${BATS_TEST_TMPDIR}/api/v1"
  echo '{"login": "test-user-2"}' >"${BATS_TEST_TMPDIR}/api/v1/user"

  export WCP_FORGE="file://${BATS_TEST_TMPDIR}"

  run ${WCP} db pull -n
  assert_output --partial "${WCP_FORGE}/api/packages/test-user-2/generic/wcp-database/current/db.sql"
}

@test "database merge works" {
  mkdir -p "${BATS_TEST_TMPDIR}/api/v1"
  echo '{"login": "test-user-2"}' >"${BATS_TEST_TMPDIR}/api/v1/user"

  export WCP_FORGE="file://${BATS_TEST_TMPDIR}"

  run ${WCP} db push -n
  run ${WCP} db merge -n
  assert_output --partial "INSERT OR IGNORE INTO commits"
  assert_output --partial "INSERT OR IGNORE INTO checklist"
  assert_output --partial "INSERT OR IGNORE INTO links"
  assert_output --partial "INSERT OR IGNORE INTO session"
}

@test "database migrations work" {
  ${WCP} session start --source=gitea

  _libdir="${BATS_CWD}/scripts/wcp"
  . "${BATS_CWD}/scripts/wcp/db.sh"

  versions="$(sqlite3 "${_WCP_DBFILE}" "SELECT applied FROM versions ORDER BY applied ASC;")"
  expected_versions="$(seq 1 ${DB_CURRENT_VERSION})"
  assert_equal "${expected_versions}" "${versions}"
}
