# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

setup() {
  load "common/setup.sh"
}

teardown() {
  load "common/teardown.sh"
}

@test "main help works" {
  run ${WCP} help
  assert_output --partial "wcp COMMAND ..."
}

@test "help <subcommand> works" {
  run ${WCP} help session
  assert_output --partial "wcp session [--id=<ID>] <SUBCOMMAND> ..."

  run ${WCP} help commit
  assert_output --partial "wcp [commit] [<COMMIT-ID>] [+N|-N] <SUBCOMMAND> ..."

  run ${WCP} help db
  assert_output --partial "wcp db <SUBCOMMAND> ..."

  run ${WCP} help notes
  assert_output --partial "## Notes"
}

@test "help <subcommand> == <subcommand> help" {
  do_test() {
    run ${WCP} ${1} help
    assert_output "$(${WCP} help ${1})"
  }

  do_test session
  do_test commit
  do_test notes
  do_test db
  do_test help
}

@test "session checklist help" {
  run ${WCP} session checklist help
  assert_output --partial "wcp session checklist <SUBCOMMAND> ..."
}

@test "session pr help" {
  run ${WCP} session pr help
  assert_output --partial "wcp session pr <SUBCOMMAND> ..."
}

@test "session ci help" {
  run ${WCP} session ci help
  assert_output --partial "wcp session ci <SUBCOMMAND> ..."
}

@test "help unknown fails" {
  run -127 ${WCP} help unknown
  assert_output --partial "cmd_wcp_unknown_help: command not found"
}
