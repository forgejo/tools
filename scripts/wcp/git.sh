# -*- mode: bash -*-
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

. "${_libdir}/git/commits.sh"

git_ref_get() {
  local ish
  ish="${1}"

  git rev-parse --revs-only "${ish}"
}

git_branch_create_and_switch() {
  local new_branch base_branch
  new_branch="${1}"
  base_branch="${2}"

  git switch -C "${new_branch}" "${base_branch}"
}

git_branch_exists() {
  local branch
  branch="${1}"

  git rev-parse --verify --quiet "${branch}" >/dev/null
}

git_cherry_pick() {
  local commit_id
  commit_id="${1}"

  git cherry-pick -x "${commit_id}"
}

git_cherry_pick_in_progress() {
  git rev-parse -q --verify CHERRY_PICK_HEAD >/dev/null
}

git_current_branch() {
  git branch --show-current
}

assert_correct_branch() {
  local session_id
  session_id="$1"

  week="$(db_session_week_get "${session_id}")"
  expected_branch="wcp/${week}"

  if [ "${expected_branch}" != "$(git_current_branch)" ]; then
    echo "Error: Current branch (\`$(git_current_branch)\`) is not \`${expected_branch}\`." >&2
    exit 1
  fi
}
