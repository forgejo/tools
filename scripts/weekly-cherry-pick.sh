#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

set -euo pipefail

_libdir="$(dirname "${0}")/wcp"
# shellcheck source-path=./scripts/wcp
. "${_libdir}/lib.sh"

main() {
  local cmd

  mkdir -p "${STATE_DIR}" "${CONFIG_DIR}"
  if [ -e "${CONFIG_FILE}" ]; then
    # shellcheck disable=SC1090
    . "${CONFIG_FILE}"
  fi
  WCP_FORGE_API_TOKEN="${WCP_FORGE_API_TOKEN:-${WCP_PULL_REQUEST_API_TOKEN:-}}"
  db_init

  cmd="${1:-}"
  shift || true

  case "${cmd}" in
    session | s)
      cmd_wcp_session "$@"
      ;;
    help | h)
      cmd_wcp_help "$@"
      ;;
    commit | c)
      cmd_wcp_commit "$@"
      ;;
    notes | n)
      cmd_wcp_notes_help
      ;;
    db)
      cmd_wcp_db "$@"
      ;;
    *)
      cmd_wcp_commit "${cmd}" "$@"
      ;;
  esac
}

if [ "${VERBOSE:-0}" != "0" ]; then
  set -x
  set +u
  PS4='${BASH_SOURCE[0]}:$LINENO: ${FUNCNAME[0]}:  '
fi

if [ -n "${WCP_INIT_FILE:-}" ]; then
  # shellcheck disable=SC1090
  . "${WCP_INIT_FILE}"
fi

main "$@"
