# SPDX-FileCopyrightText: 2024 Gergely Nagy
# SPDX-FileContributor: Gergely Nagy
#
# SPDX-License-Identifier: EUPL-1.2

nproc := `nproc`

[private]
default: help

help:
    @just --list

lint:
    reuse lint -q
    shellcheck -a -x scripts/weekly-cherry-pick.sh

check-fmt:
    shfmt -i 2 -ci -d scripts/

fmt:
    shfmt -i 2 -ci -l -w scripts/

check *ARGS:
    bats -r scripts/wcp/t -j {{nproc}} {{ARGS}}
